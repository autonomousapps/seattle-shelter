package com.seattleshelter.entities

data class PetDetails(
    val name: String,
    val picUrls: List<String>,
    val videoUrl: String?,
    val description: String?,
    val id: Long?,
    val sex: String?,
    val breed: String?,
    val age: String?,
    val size: String?,
    val color: String?,
    val altered: Boolean?,
    val declawed: Boolean?,
    val inFosterCare: Boolean?,
    val adoptionPending: Boolean?
)
