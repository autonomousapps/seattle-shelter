Heroku app at https://dashboard.heroku.com/apps/seattleanimalshelter

Run `git push heroku master` to update app online.

App lives at https://seattleanimalshelter.herokuapp.com/

# Current endpoints
1. /
1. /cats
1. /cats/:id
1. /dogs
1. /dogs/:id
1. /rabbits
1. /rabbits/:id
1. /smallmammals
1. /smallmammals/:id
1. /reptiles
1. /reptiles/:id

# TODO
1. Add an auth system?
