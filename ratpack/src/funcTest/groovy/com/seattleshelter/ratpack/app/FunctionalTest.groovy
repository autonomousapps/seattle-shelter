package com.seattleshelter.ratpack.app

import com.fasterxml.jackson.databind.ObjectMapper
import com.fasterxml.jackson.module.kotlin.KotlinModule
import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import groovy.json.JsonSlurper
import ratpack.test.MainClassApplicationUnderTest
import spock.lang.AutoCleanup
import spock.lang.Specification

class FunctionalTest extends Specification {

    private static final jsonSlurper = new JsonSlurper()
    private static final objectMapper = new ObjectMapper().registerModule(new KotlinModule())

    @AutoCleanup
    def api = new MainClassApplicationUnderTest(Main)

    def "can get api details"() {
        when: 'We hit the root API'
        def response = api.httpClient.get()

        then: 'The API is the response'
        def apiResponse = jsonSlurper.parseText(response.body.text) as List<String>
        apiResponse == [
            "cats",
            "cats/:id",
            "dogs",
            "dogs/:id",
            "rabbits",
            "rabbits/:id",
            "smallmammals",
            "smallmammals/:id",
            "reptiles",
            "reptiles/:id"
        ]

        and: 'All list endpoints return 200'
        Map<String, List<Pet>> allPets = [:]
        apiResponse.findAll { !it.contains(":id") }.each {
            response = api.httpClient.get(it)
            assert response.statusCode == 200

            def pets = jsonSlurper.parseText(response.body.text) as List<Pet>
            allPets.put(it, pets)
        }

        and: 'All :id endpoints return 200 for one ID'
        apiResponse.findAll { it.contains(":id") }.each {
            def species = it.split("/")[0]
            def id = allPets.get(species).first().id as String
            assert api.httpClient.get(it.replaceFirst(":id", id)).statusCode == 200
        }
    }

    def "can get cats"() {
        when: 'We get the list of cats'
        def response = api.httpClient.get("cats")

        then: 'There is a reasonable number of cats'
        def cats = jsonSlurper.parseText(response.body.text) as List<Pet>
        cats.size() > 10 && cats.size() < 100

        and: 'Every cat has an ID'
        def ids = cats.collect { it.id }
        null == ids.find { it == null }

        when: 'We check roughly half the list'
        def details = ids
            .findAll { it % 2 == 0 }
            .collect { parsePetDetails(api.httpClient.get("cats/$it").body.text) }

        then: 'Cat details are available for a subset of cats'
        !details.isEmpty()

        and: 'No null ages'
        def ages = details.collect { it.age }
        null == ages.find { it == null }
    }

    def "can get dogs"() {
        when: 'We get the list of dogs'
        def response = api.httpClient.get("dogs")

        then: 'There is a reasonable number of dogs'
        def dogs = jsonSlurper.parseText(response.body.text) as List<Pet>
        dogs.size() > 10 && dogs.size() < 100

        and: 'Every dog has an ID'
        def ids = dogs.collect { it.id }
        null == ids.find { it == null }

        when: 'We check roughly half the list'
        def details = ids
            .findAll { it % 2 == 0 }
            .collect { parsePetDetails(api.httpClient.get("dogs/$it").body.text) }

        then: 'Dog details are available for a subset of dogs'
        !details.isEmpty()

        and: 'No null ages'
        def ages = details.collect { it.age }
        null == ages.find { it == null }
    }

    def "can get rabbits"() {
        when: 'We get the list of rabbits'
        def response = api.httpClient.get("rabbits")

        then: 'There is a reasonable number of rabbits'
        def rabbits = jsonSlurper.parseText(response.body.text) as List<Pet>
        rabbits.size() > 2 && rabbits.size() < 100

        and: 'Every rabbits has an ID'
        def ids = rabbits.collect { it.id }
        null == ids.find { it == null }

        when: 'We check roughly half the list'
        def details = ids
            .findAll { it % 2 == 0 }
            .collect { parsePetDetails(api.httpClient.get("rabbits/$it").body.text) }

        then: 'Rabbit details are available for a subset of rabbits'
        !details.isEmpty()

        and: 'No null ages'
        def ages = details.collect { it.age }
        null == ages.find { it == null }
    }

    def "can get small mammals"() {
        when: 'We get the list of small mammals'
        def response = api.httpClient.get("smallmammals")

        then: 'There is a reasonable number of small mammals'
        def smallMammals = jsonSlurper.parseText(response.body.text) as List<Pet>
        smallMammals.size() > 10 && smallMammals.size() < 100

        and: 'Every small mammal has an ID'
        def ids = smallMammals.collect { it.id }
        null == ids.find { it == null }

        when: 'We check roughly half the list'
        def details = ids
            .findAll { it % 2 == 0 }
            .collect { parsePetDetails(api.httpClient.get("smallmammals/$it").body.text) }

        then: 'Small mammal details are available for a subset of small mammals'
        !details.isEmpty()

        and: 'No null ages'
        def ages = details.collect { it.age }
        null == ages.find { it == null }
    }

    def "can get reptiles"() {
        when: 'We get the list of reptiles'
        def response = api.httpClient.get("reptiles")

        then: 'There is a reasonable number of reptiles'
        def reptiles = jsonSlurper.parseText(response.body.text) as List<Pet>
        reptiles.size() > 0 && reptiles.size() < 10

        and: 'Every reptile has an ID'
        def ids = reptiles.collect { it.id }
        null == ids.find { it == null }

        when: 'We check the list'
        def details = ids
            .collect { parsePetDetails(api.httpClient.get("reptiles/$it").body.text) }

        then: 'Reptile details are available'
        !details.isEmpty()

        and: 'No null ages'
        def ages = details.collect { it.age }
        null == ages.find { it == null }
    }

    private static PetDetails parsePetDetails(String text) {
        return objectMapper.readValue(text, PetDetails)
    }
}
