package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.DogService

class DogsByIdHandler extends PetsByIdHandler<DogService> {

    DogsByIdHandler() {
        super(DogService)
    }
}
