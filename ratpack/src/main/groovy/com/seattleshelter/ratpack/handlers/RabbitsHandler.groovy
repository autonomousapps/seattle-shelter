package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.RabbitService

class RabbitsHandler extends PetsHandler<RabbitService> {

    RabbitsHandler() {
        super(RabbitService)
    }
}
