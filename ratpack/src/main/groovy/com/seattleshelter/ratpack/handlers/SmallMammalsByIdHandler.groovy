package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.SmallMammalService

class SmallMammalsByIdHandler extends PetsByIdHandler<SmallMammalService> {

    SmallMammalsByIdHandler() {
        super(SmallMammalService)
    }
}
