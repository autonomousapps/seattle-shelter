package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.SmallMammalService

class SmallMammalsHandler extends PetsHandler<SmallMammalService> {

    SmallMammalsHandler() {
        super(SmallMammalService)
    }
}
