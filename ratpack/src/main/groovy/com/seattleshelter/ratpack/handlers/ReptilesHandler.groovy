package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.ReptilService

class ReptilesHandler extends PetsHandler<ReptilService> {

    ReptilesHandler() {
        super(ReptilService)
    }
}
