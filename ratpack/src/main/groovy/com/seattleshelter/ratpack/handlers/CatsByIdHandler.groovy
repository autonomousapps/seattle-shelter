package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.CatService
import groovy.util.logging.Slf4j

@Slf4j
class CatsByIdHandler extends PetsByIdHandler<CatService> {

    CatsByIdHandler() {
        super(CatService)
    }
}
