package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.PetService
import ratpack.handling.Context
import ratpack.handling.Handler

import static groovy.json.JsonOutput.toJson

abstract class PetsHandler<T extends PetService> implements Handler {

    private final Class<T> serviceClass

    PetsHandler(Class<T> serviceClass) {
        this.serviceClass = serviceClass
    }

    @Override
    void handle(Context ctx) throws Exception {
        T service = ctx.get(serviceClass)

        service.pets.then { pets ->
            ctx.render toJson(pets)
        }
    }
}
