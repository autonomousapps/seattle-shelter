package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.CatService

class CatsHandler extends PetsHandler<CatService> {

    CatsHandler() {
        super(CatService)
    }
}
