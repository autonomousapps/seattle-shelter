package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.PetService
import groovy.util.logging.Slf4j
import ratpack.handling.Context
import ratpack.handling.Handler

import static groovy.json.JsonOutput.toJson
import static ratpack.jackson.Jackson.json

@Slf4j
abstract class PetsByIdHandler<T extends PetService> implements Handler {

    private final Class<T> serviceClass

    PetsByIdHandler(Class<T> serviceClass) {
        this.serviceClass = serviceClass
    }

    @Override
    void handle(Context ctx) throws Exception {
        T shelter = ctx.get(serviceClass)

        def id = ctx.pathTokens.asLong("id")
        log.info "id = $id"
        if (id != null) {
            shelter.getPetById(id).then { pet ->
                if (pet != null) {
                    log.info "pet = $pet"
                    ctx.render toJson(pet)
                } else {
                    ctx.response.status(404).send()
                }
            }
        } else {
            ctx.response.status(400)
            ctx.render(json([status: "error", message: "id is required"]))
        }
    }
}
