package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.DogService

class DogsHandler extends PetsHandler<DogService> {

    DogsHandler() {
        super(DogService)
    }
}
