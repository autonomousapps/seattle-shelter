package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.RabbitService

class RabbitsByIdHandler extends PetsByIdHandler<RabbitService> {

    RabbitsByIdHandler() {
        super(RabbitService)
    }
}
