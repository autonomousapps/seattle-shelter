package com.seattleshelter.ratpack.handlers

import com.seattleshelter.parser.service.ReptilService
import groovy.util.logging.Slf4j

@Slf4j
class ReptilesByIdHandler extends PetsByIdHandler<ReptilService> {

    ReptilesByIdHandler() {
        super(ReptilService)
    }
}
