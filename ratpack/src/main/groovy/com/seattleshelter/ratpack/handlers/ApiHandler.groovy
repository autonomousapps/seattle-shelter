package com.seattleshelter.ratpack.handlers

import ratpack.handling.Context
import ratpack.handling.Handler

import static groovy.json.JsonOutput.toJson

class ApiHandler implements Handler {

    private static final List<String> API = [
        "cats",
        "cats/:id",
        "dogs",
        "dogs/:id",
        "rabbits",
        "rabbits/:id",
        "smallmammals",
        "smallmammals/:id",
        "reptiles",
        "reptiles/:id"
    ]

    @Override
    void handle(Context ctx) throws Exception {
        ctx.render toJson(API)
    }
}
