package com.seattleshelter.ratpack.app

import com.seattleshelter.parser.WebParser
import com.seattleshelter.parser.document.WebDocumentProvider
import com.seattleshelter.parser.service.*
import com.seattleshelter.ratpack.handlers.*
import ratpack.groovy.Groovy
import ratpack.handling.RequestLogger
import ratpack.server.RatpackServer

class Main {
    static void main(String[] args) {
        RatpackServer.start { spec ->
            spec.registryOf { r ->
                // Services
                def parser = new WebParser(new WebDocumentProvider())
                r.add CatService, new CatService(parser)
                r.add DogService, new DogService(parser)
                r.add RabbitService, new RabbitService(parser)
                r.add SmallMammalService, new SmallMammalService(parser)
                r.add ReptilService, new ReptilService(parser)

                // Handlers
                r.add CatsHandler, new CatsHandler()
                r.add CatsByIdHandler, new CatsByIdHandler()

                r.add DogsHandler, new DogsHandler()
                r.add DogsByIdHandler, new DogsByIdHandler()

                r.add RabbitsHandler, new RabbitsHandler()
                r.add RabbitsByIdHandler, new RabbitsByIdHandler()

                r.add SmallMammalsHandler, new SmallMammalsHandler()
                r.add SmallMammalsByIdHandler, new SmallMammalsByIdHandler()

                r.add ReptilesHandler, new ReptilesHandler()
                r.add ReptilesByIdHandler, new ReptilesByIdHandler()

                r.add ApiHandler, new ApiHandler()
            }
            spec.handlers(Groovy.chain {
                all(RequestLogger.ncsa())
                prefix('cats') {
                    get(CatsHandler)
                    get(':id', CatsByIdHandler)
                }
                prefix('dogs') {
                    get(DogsHandler)
                    get(':id', DogsByIdHandler)
                }
                prefix('rabbits') {
                    get(RabbitsHandler)
                    get(':id', RabbitsByIdHandler)
                }
                prefix('smallmammals') {
                    get(SmallMammalsHandler)
                    get(':id', SmallMammalsByIdHandler)
                }
                prefix('reptiles') {
                    get(ReptilesHandler)
                    get(':id', ReptilesByIdHandler)
                }
                get(ApiHandler)
            })
        }
    }
}
