package com.seattleshelter.parser.document

import com.seattleshelter.parser.test.Website
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

class StringDocumentProvider implements DocumentProvider {

    @Override
    Document getListFor(WebDocumentProvider.Pet pet) throws IOException {
        String url
        switch (pet) {
            case WebDocumentProvider.Pet.Cat:
                url = Website.HTML_CATS
                break
            case WebDocumentProvider.Pet.Dog:
                url = Website.HTML_DOGS
                break
            case WebDocumentProvider.Pet.Rabbit:
                url = Website.HTML_RABBITS
                break
            case WebDocumentProvider.Pet.SmallFurry:
                url = Website.HTML_SMALL_MAMMALS
                break
            case WebDocumentProvider.Pet.Reptile:
                url = Website.HTML_REPTILES
                break
            default:
                throw new IllegalArgumentException("No support for ${pet.name()} yet")
        }

        return Jsoup.parse(url)
    }

    @Override
    Document getDetailsFor(long id) throws IOException {
        String url
        switch (id) {
            case 29127858:
                url = Website.OREO_DETAILS
                break
            case 29000199:
                url = Website.SQUID_DETAILS
                break
            case 37767290:
                url = Website.BUDDY_DETAILS
                break
            case 40839555:
                url = Website.TOFU_DETAILS
                break
            case 40415293:
                url = Website.DAENERYS_DETAILS
                break
            case 39763006:
                url = Website.PUFF_DETAILS
                break
            default:
                throw new IllegalArgumentException("ID <$id> is not supported")
        }

        return Jsoup.parse(url)
    }
}
