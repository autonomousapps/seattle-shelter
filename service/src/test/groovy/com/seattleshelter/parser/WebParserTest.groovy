package com.seattleshelter.parser

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.document.DocumentProvider
import com.seattleshelter.parser.document.StringDocumentProvider
import spock.lang.Specification

class WebParserTest extends Specification {

    private final DocumentProvider documentProvider = new StringDocumentProvider()
    private final WebParser webParser = new WebParser(documentProvider)

    def "can parse cat list"() {
        when:
        def cats = webParser.parseCats()

        then:
        cats.size() == allCats.size()
        cats.collect { it.name } == allCats.collect { it.name }
    }

    def "parsing cat details works"() {
        when:
        def id = 29127858L
        def oreo = webParser.parseCat(id)

        then:
        oreo == OREO
    }

    def "parsing dog list works"() {
        when:
        def dogs = webParser.parseDogs()

        then: 'There are 21 dogs with the expected names'
        dogs.size() == 21
        dogs.collect { it.name } == [
            'Squid', 'Peanut', 'Elias (Eli)', 'Rosa', 'Cairo', 'Buddy', 'Stella', 'Emme', 'Ziggy', 'Leonitus Cornelius',
            'Little Bella Renault', 'Edina', 'Annie', 'Cash', 'Malia', 'Lulabelle', 'Bella', 'Abaco', 'Morgan', 'Ellen',
            'Nala'
        ]

        and: 'Each as a proper ID'
        dogs.collect { it.id }.find { it == -1 } == null
    }

    def "parsing dog details works"() {
        when:
        def id = 29000199
        def squid = webParser.parseDog(id)

        then:
        squid == new PetDetails(
            'Squid',
            ['http://g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8.jpg', 'http://g.petango.com/photos/2681/b48a9d84-9786-4c0c-8c5c-5691ed657d39.jpg', 'http://g.petango.com/photos/2681/174dc51a-abfc-42a4-8c6b-f5abc1f01782.jpg'],
            '',
            'Squid is in a foster home, and is currently not at the shelter. Please find info below on how to adopt her, thanks. Squid is a busy, brown-eyed girl who\'s a great companion for leisurely walks. At 12 years young, this pretty brindle girl will thrive in a moderately active home with adults who already have dog experience. Squid knows "sit" and is house-trained. With a guiding hand and a bit of patience, she\'s eager to learn more! She would love to be your only pet. Come meet this loving yet independent lady, and see if she\'s the one for you! HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we\'re open ONLY for redeeming lost pets, from 2 pm - 4 pm. IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!',
            id,
            'Female',
            'Terrier, American Pit Bull/Mix',
            '12 years 4 months 24 days',
            'Medium',
            'Brindle',
            true,
            false,
            true,
            false
        )
    }

    def "parsing dog details with video works"() {
        when:
        def id = 37767290
        def buddy = webParser.parseDog(id)

        then:
        buddy == new PetDetails(
            'Buddy',
            ['http://g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf.jpg', 'http://g.petango.com/photos/2681/7ab24826-e644-42c6-ba35-c1d0da831c98.jpg', 'http://g.petango.com/photos/2681/6b12b1a9-cfcd-4a01-b668-3cf6fb93738b.jpg'],
            'https://youtu.be/s6j7024jHfw',
            '** CHECK OUT BUDDY\'S NEW VIDEO HERE https://youtu.be/s6j7024jHfw ** Buddy is in a foster home and is currently not at the shelter. Please see info below on how to adopt him - thanks Dynamic Dog Seeks Forever Family: Me: Athletic and energetic, I love playing with other dogs. I\'m a little over a year old, and a tall, lovable mix of Ibizan Hound and some other good stuff! I\'m a great hiking/jogging/fast walking partner, and a real life, "talking" doorbell: which translates to, I\'ll let you know when we have guests! At the end of a productive day, I want to lay in your lap! I\'m shy with new people but get attached quickly. I\'m a leggy 65 pounds or so. You: Can provide the active lifestyle I need, plenty of chew toys, and have experience using reward-based training techniques. I\'d love a yard to play games with my dog sibling and/or friends. A committed, invested family is very important! Kids make me a little uneasy, but with slow introductions, I\'d probably welcome a dog-savvy, feline sibling. (and did I mention I like dogs??) If you like what you\'ve read about me, and "Match" my needs, please send an application. Thanks! HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we\'re open ONLY for redeeming lost pets, from 2 pm - 4 pm. IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!',
            id,
            'Male',
            'Ibizan Hound/Mix',
            '2 years 1 month 4 days',
            'Large',
            'Red',
            true,
            false,
            true,
            false
        )
    }

    def "parsing rabbit list works"() {
        when:
        def rabbits = webParser.parseRabbits()

        then: 'There are 12 rabbits with the expected names'
        rabbits.size() == 12
        rabbits.collect { it.name } == [
            'Zoey', 'Bonny', 'Teddy', 'Serena', 'Joey', 'Pixie', 'Nia', 'Judy Hopps', 'Freya', 'Hennessy', 'Daikon',
            'Tofu'
        ]

        and: 'Each as a proper ID'
        rabbits.collect { it.id }.find { it == -1 } == null
    }

    def "parsing rabbit details works"() {
        when:
        def id = 40839555
        def tofu = webParser.parseRabbit(id)

        then:
        tofu == new PetDetails(
            'Tofu',
            ['http://g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669.jpg', 'http://g.petango.com/photos/2681/b31bf1f8-04bf-404c-8ec9-b17dc509b6c9.jpg', 'http://g.petango.com/photos/2681/55623bc9-39d2-4456-a496-ffa63ada1a85.jpg'],
            '',
            'Friendly, curious, and fluffy! That\'s Daikon and Tofu! Daikon and Tofu are Holland Lop adult rabbits who are fully grown three-year-old siblings. Daikon is the male grey rabbit and his sister Tofu is the brown rabbit. Tofu and Daikon are a bonded pair and will be adopted together. They both have beautiful fur; Daikon\'s is short and velvety soft to the touch and Tofu\'s is fluffier and super soft. These are two healthy siblings have been microchipped and spayed and are ready to become part of your family. After an afternoon of playtime, Daikon and Tofu wanted you to know that they like to hop around and stretch their legs and may even lounge with/on you while watching TV. Their temperament seems to be a bit shy at first but eventually becoming confident and curious. They get along well with people and each other and tolerate petting and some handling. They have both lived in a home with cats. Daikon and Tofu are looking forward to Springing Ahead! To meet you and going home to your loving forever home. HOW TO ADOPT: Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week, Wednesday-Sunday, 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays. HOUSE RABBIT CARE: Housing requirements for house rabbits should be a metal exercise pen set up in a formation that allows the rabbit to hop a few times in each direction (at least 8 square feet for a single rabbit). Depending on how high the rabbit can jump, the pen may need to be 3 feet to 4 feet tall. Your rabbit\'s enclosure should include at least one litter box with fresh hay, food and water bowls, and a refuge where the rabbit can get out of sight, such as a cardboard box with multiple holes cut out work, soft places to lie down and toys for enrichment. Your rabbit should get at least one to two hours daily outside its pen in a larger space (at least 24 square feet). Rabbits are healthiest when they have lots of space to run, jump, play and interact with you. Make sure any room your rabbit visits is "bunny-proofed." Hide or remove electrical wires and remove toxic house plants. Rabbits are intelligent and social animals; they do best with a rabbit companion and need daily interaction with their human friends. Rabbits should be fed unlimited hay, high-quality hay pellets, and fresh leafy greens. (Leaf lettuce, carrot tops, parsley, cilantro, and pesticide-free dandelion are good choices.) Treats include small portions of carrot or fruit, such as apple, berries, pear, grape, or banana. You are expected to make a lifetime commitment to your rabbit, and they should be treated as an integral part of the family. The primary caregiver must be a responsible adult, and young children should always be supervised when interacting with your rabbit and instructed on proper approach and handling. Household cats or dogs should, of course, also be carefully supervised at all times and introduced in a calm and quiet manner. Your rabbit will need regular nail trims and annual vet checks. With proper care and lots of love, rabbits can live to be 10-12 years old.',
            id,
            'Female',
            'American',
            '3 years 15 days',
            'Medium',
            'Brown',
            true,
            false,
            false,
            false
        )
    }

    def "parsing small mammal list works"() {
        when:
        def smallMammals = webParser.parseSmallMammals()

        then: 'There are 20 small mammals with the expected names'
        smallMammals.size() == 20
        smallMammals.collect { it.name } == [
            'Zoe', 'Abby', 'Daenerys', 'Olenna', 'Meera', 'Brienne', 'Ygritte', 'Margaery', 'Yara', 'Abigail', 'Xena',
            'Winonna', 'Winter', 'Veronica', 'Tamsin', 'Sally', 'Book', 'Biscuit', 'Debra', 'Popcorn'
        ]

        and: 'Each as a proper ID'
        smallMammals.collect { it.id }.find { it == -1 } == null
    }

    def "parsing small mammals details works"() {
        when:
        def id = 40415293
        def tofu = webParser.parseSmallMammal(id)

        then:
        tofu == new PetDetails(
            'Daenerys',
            ['http://g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb.jpg', 'http://g.petango.com/photos/2681/b1c6d810-ed92-4a16-84e4-1737ee4b8773.jpg', 'http://g.petango.com/photos/2681/8a37ec67-5e29-47fd-b8e8-5f3f813ca750.jpg'],
            '',
            'Meet Daenerys Hamsterborn! Don\'t worry about dragons with this lady. They are tamed and well-hidden within her carefresh. You\'ll never know they are around! Daenerys is a happy 7-month-old Black Bear hamster who is friendly, easy to handle, loves running on her wheel, and is looking for a new furever home! Come meet Daenerys at the Seattle Animal Shelter Critter room Wed-Sun 1-6pm! HOW TO ADOPT: Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays. HAMSTER CARE: HOUSING: Adult hamsters need to live alone, never house them with another hamster. You\'ll need to house your hamster in a cage or a tank. Glass tanks are preferable because they are safer than cages. The minimum recommended size for a Syrian or Teddy Bear hamster is 24 inches by 12 inches, and 12 inches high. If you are getting a cage, ensure the bars lining the cage aren\'t too big for your dwarf hamster to squeeze through. Make sure that you have a suitable location to place your hamster\'s new home. The location should be away from direct sunlight or draft as well as quiet and safe from potential predators (including other pets such as cats and dogs). Do note that your dwarf hamster is nocturnal and will be squeaking, scratching, or using the wheel a lot at night, so your bedroom might not be the best place. The tank or cage bottom should be covered with bedding that is non-toxic and comfortable for your new hamster. Paper bedding is recommended. The bedding should be around 2-3 inches in depth and your hamster will move it around as they see fit while they are nesting. Cedar and pine shavings should not be used as they may cause your pet to develop respiratory problems such as asthma. You will need to remove all the bedding, old food, and completely clean the tank every week. Also, clean the wheel and any toys too. It is important that you get a solid plastic wheel, 9\'\' or 12\'\' in diameter to run on. Do not use metal wheels with openings as these can cause injuries. A "saucer" wheel also works well for hamsters. Other things you will need in the cage are a hanging water bottle, small food dish, a house or other hiding place, some fresh hay, and chew blocks or toys. EXERCISE: Hamsters have tons of energy and need to run on their wheel every day. Make certain that the wheel is able to spin freely, as it sometimes gets stuck into the bedding. If the wheel can\'t move this can stress or harm the hamster. Consider getting a "tank topper" for your glass tank so you can add ramps, extra hiding places and more room to explore in. Plastic hamster balls are also a fun way to exercise your hamster. They should be 12 inches in diameter and limit their time inside to about 5-10 minutes. HANDLING: Start by talking gently to your hamster so it becomes used to your voice. Introduce your hand into the cage with a treat and allow your hamster to approach you. Remember hamsters are easily startled. Take extra care when wakening them up, as they may get scared and nip at you. Lift handle your hamster gently. Scoop them up in both hands and hold them with cupped hands so they\'ll feel safe and secure. Keep your hands over a table or close to the floor in case your hamster drops out! FOOD: It is recommended that you feed your hamster a pelleted hamster food. Pelleted foods will give your hamster optimal nutrition because pellets contain a healthy blend of grains, seeds, vegetables, fruits, proteins, vitamins, and minerals. A small handful of fresh grass hay is recommended. Treats should be fed very sparingly. Use sugar-free treats like rolled oats or cereal "O\'s". Small amounts of leafy greens, dandelion, lettuce, cilantro are good choices. A couple of pieces of corn or peas are great treats too. Limit fruit to very small pieces, a few times a week, about the size of a blueberry, or half of a raspberry. A Syrian hamster lifespan 2 to 3 years. The adults grow to about 5 to 7 inches long and should weigh about 5 to 7 ounces.',
            id,
            'Female',
            'Hamster/Hamster',
            '8 months 20 days',
            'Large',
            'Black',
            false,
            false,
            false,
            false
        )
    }

    def "parsing reptile list works"() {
        when:
        def reptiles = webParser.parseReptiles()

        then: 'There are 2 reptiles with the expected names'
        reptiles.size() == 2
        reptiles.collect { it.name } == ['Puff', 'Granite']

        and: 'Each as a proper ID'
        reptiles.collect { it.id }.find { it == -1 } == null
    }

    def "parsing reptile details works"() {
        when:
        def id = 39763006
        def puff = webParser.parseSmallMammal(id)

        then:
        puff == new PetDetails(
            'Puff',
            ['http://g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8.jpg', 'http://g.petango.com/photos/2681/bd1e57a1-919a-489d-9ba0-43420863894c.jpg', 'http://g.petango.com/photos/2681/55cc0a22-5cd2-4d32-86d8-3ad5179d76c2.jpg'],
            '',
            'Puff is an amazing Bearded Dragon and he is looking for a great new forever home! Puff is very shy and a little bit unsocialized so he will need a lot of patience and time to get him to be the wonderful beardie that he is! Because of his special needs, he will have to be adopted by an experienced beardie family! Puff is currently being adored in foster care! HOW TO ADOPT: For foster animals, please fill out a critter adoption form available at the shelter. Then you must meet with an animal care officer IN PERSON at the shelter to review your application. Then we will arrange a day and time when the foster parent and critter will meet you at the Shelter critter room. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and legal holidays. After the review, please allow 2-3 business days for the foster parent to reach out to you to set up a meet and greet.',
            id,
            'Male',
            'Lizard',
            '',
            'Medium',
            'Grey/Tan',
            false,
            false,
            true,
            false
        )
    }

    private static final PetDetails OREO = new PetDetails(
        "Oreo", [
        "http://g.petango.com/photos/2681/239232bf-182c-4149-9c1a-23f259e8cbf8.jpg",
        "http://g.petango.com/photos/2681/e44d6521-7dbf-4295-9076-ac4999f8c942.jpg",
        "http://g.petango.com/photos/2681/34e87548-b035-4096-b765-3e0bea7c912e.jpg"
    ],
        '',
        "**Important Note** You won't find this kitty at the shelter. This kitty is waiting patiently in foster care for your call. Please contact the Foster Care Hotline at 206-684-0685 to arrange a meeting. Thank you! Oreo is a talkative chonkster who has earned her nickname of \"Double Stuf\". Treats and regular meals are the keys to her heart. She keeps active with food puzzle toys. Her scratching posts must be sturdy and stable to withstand her powerful (if uncoordinated) assaults. Sacrifice mousies and feathers to her frequently to earn her benevolence. Oreo was quite anxious at the shelter, but gradually came out of her shell in foster care. Let her approach you and you'll be rewarded with head bonks and chirrups. She loves a good head scratch but is easily overstimulated, so don't be offended by a hiss - just lay off and she'll return when she's ready. She'd probably be best with experienced cat people with no small children, dogs, or other cats.",
        29127858L,
        "Female",
        "Domestic Shorthair",
        "4 years 7 months 3 days",
        "Medium",
        "Brown/White",
        true,
        false,
        true,
        false
    )

    private final List<Pet> allCats = [
        JASMINE, COTTONBALL, ED, BUDDY, SCRAPPY
    ]

    private static final Pet JASMINE = new Pet(
        "Jasmine",
        "http://g.petango.com/photos/2681/38b36e3a-0e07-4538-a0eb-b4745482f1a7_TN1.jpg",
        29043338,
        "Female/Spayed",
        "Domestic Shorthair",
        "10 years",
        true,
        false
    )

    private static final Pet COTTONBALL = new Pet(
        "Cottonball",
        "http://g.petango.com/photos/2681/e38dc2b0-9797-459d-b33f-8eedc029f2c4_TN1.jpg",
        29174409,
        "Female",
        "Domestic Shorthair",
        "",
        true,
        false
    )

    private static final Pet ED = new Pet(
        "Ed",
        "http://g.petango.com/photos/2681/590af9c7-2eb6-4d78-b41e-9d57eb6e526c_TN1.jpg",
        29193651,
        "Male/Neutered",
        "Domestic Shorthair",
        "11 years 11 months",
        true,
        false
    )

    private static final Pet BUDDY = new Pet(
        "Buddy",
        "http://g.petango.com/photos/2681/a0666228-ba18-4e14-91e6-db7d7877dcb3_TN1.jpg",
        35002069,
        "Male/Neutered",
        "Domestic Shorthair",
        "15 years 7 months",
        true,
        false
    )

    private static final Pet SCRAPPY = new Pet(
        "Scrappy",
        "http://g.petango.com/photos/2681/d5bdbfb6-12dc-482c-969e-50b012b534de_TN1.jpg",
        37604030,
        "Male/Neutered",
        "Domestic Medium Hair/Mix",
        "14 years 10 months",
        false,
        false
    )
}