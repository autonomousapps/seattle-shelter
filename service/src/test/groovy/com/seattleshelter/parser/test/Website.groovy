package com.seattleshelter.parser.test

class Website {

    static HTML_CATS = "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
            "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
            "<head>\n" +
            "\n" +
            "    <title>Adoptable Animals</title>\n" +
            "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css\" type=\"text/css\" />\n" +
            "    <style type=\"text/css\">\n" +
            "        .hidden { display: none; }\n" +
            "    </style>\n" +
            "    <script language=javascript type=\"text/javascript\">\n" +
            "        var newwindow;\n" +
            "        function poptastic(url)\n" +
            "        {\n" +
            "\t        newwindow=window.open(url,'Animal_Details','height=700,width=700,scrollbars=1,resizable=1');\n" +
            "\t        if (window.focus) {newwindow.focus()}\n" +
            "        }\n" +
            "    </script>\n" +
            "</head>\n" +
            "\n" +
            "<body class=\"list-body\">\n" +
            "        <!-- Google Tag Manager -->\n" +
            "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
            "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
            "    <script>(function (w, d, s, l, i) {\n" +
            "    w[l] = w[l] || []; w[l].push({\n" +
            "        'gtm.start':\n" +
            "\n" +
            "        new Date().getTime(), event: 'gtm.js'\n" +
            "    }); var f = d.getElementsByTagName(s)[0],\n" +
            "\n" +
            "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
            "\n" +
            "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
            "\n" +
            "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
            "    <!-- End Google Tag Manager -->\n" +
            "\n" +
            "<div class=\"list-body\">\n" +
            "    <table id=\"tblSearchResults\" class=\"list-table\" cellspacing=\"2\" border=\"0\" style=\"width:100%\">\n" +
            "<tr>\n" +
            "<td class=\"list-item\">\n" +
            "<div class=\"list-animal-photo-block\">\n" +
            "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29043338&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/38b36e3a-0e07-4538-a0eb-b4745482f1a7_TN1.jpg\" alt=\"Photo\"></a>\n" +
            "</div>\n" +
            "<div class=\"list-animal-info-block\">\n" +
            "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29043338&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Jasmine</a></div>\n" +
            "<div class=\"list-animal-id\">29043338</div>\n" +
            "<div class=\"list-anima-species\">Cat</div>\n" +
            "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
            "<div class=\"list-animal-breed\">Domestic Shorthair</div>\n" +
            "<div class=\"list-animal-age\">10 years</div>\n" +
            "<div class=\"hidden\">Foster Care</div>\n" +
            "</div>\n" +
            "</td>\n" +
            "<td class=\"list-item\">\n" +
            "<div class=\"list-animal-photo-block\">\n" +
            "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29174409&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e38dc2b0-9797-459d-b33f-8eedc029f2c4_TN1.jpg\" alt=\"Photo\"></a>\n" +
            "</div>\n" +
            "<div class=\"list-animal-info-block\">\n" +
            "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29174409&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Cottonball</a></div>\n" +
            "<div class=\"list-animal-id\">29174409</div>\n" +
            "<div class=\"list-anima-species\">Cat</div>\n" +
            "<div class=\"list-animal-sexSN\">Female</div>\n" +
            "<div class=\"list-animal-breed\">Domestic Shorthair</div>\n" +
            "<div class=\"list-animal-age\"></div>\n" +
            "<div class=\"hidden\">Foster Care</div>\n" +
            "</div>\n" +
            "</td>\n" +
            "</tr>\n" +
            "<tr>\n" +
            "<td class=\"list-item\">\n" +
            "<div class=\"list-animal-photo-block\">\n" +
            "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29193651&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/590af9c7-2eb6-4d78-b41e-9d57eb6e526c_TN1.jpg\" alt=\"Photo\"></a>\n" +
            "</div>\n" +
            "<div class=\"list-animal-info-block\">\n" +
            "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29193651&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Ed</a></div>\n" +
            "<div class=\"list-animal-id\">29193651</div>\n" +
            "<div class=\"list-anima-species\">Cat</div>\n" +
            "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
            "<div class=\"list-animal-breed\">Domestic Shorthair</div>\n" +
            "<div class=\"list-animal-age\">11 years 11 months</div>\n" +
            "<div class=\"hidden\">Foster Care</div>\n" +
            "</div>\n" +
            "</td>\n" +
            "<td class=\"list-item\">\n" +
            "<div class=\"list-animal-photo-block\">\n" +
            "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35002069&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/a0666228-ba18-4e14-91e6-db7d7877dcb3_TN1.jpg\" alt=\"Photo\"></a>\n" +
            "</div>\n" +
            "<div class=\"list-animal-info-block\">\n" +
            "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35002069&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Buddy</a></div>\n" +
            "<div class=\"list-animal-id\">35002069</div>\n" +
            "<div class=\"list-anima-species\">Cat</div>\n" +
            "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
            "<div class=\"list-animal-breed\">Domestic Shorthair</div>\n" +
            "<div class=\"list-animal-age\">15 years 7 months</div>\n" +
            "<div class=\"hidden\">Foster Care</div>\n" +
            "</div>\n" +
            "</td>\n" +
            "</tr>\n" +

            "<tr>\n" +
            "<td class=\"list-item\">\n" +
            "<div class=\"list-animal-photo-block\">\n" +
            "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37604030&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/d5bdbfb6-12dc-482c-969e-50b012b534de_TN1.jpg\" alt=\"Photo\"></a>\n" +
            "</div>\n" +
            "<div class=\"list-animal-info-block\">\n" +
            "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37604030&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Scrappy</a></div>\n" +
            "<div class=\"list-animal-id\">37604030</div>\n" +
            "<div class=\"list-anima-species\">Cat</div>\n" +
            "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
            "<div class=\"list-animal-breed\">Domestic Medium Hair/Mix</div>\n" +
            "<div class=\"list-animal-age\">14 years 10 months</div>\n" +
            "<div class=\"hidden\">Cat Free Roaming Room</div>\n" +
            "</div>\n" +
            "</td>\n" +

            "</tr>\n" +

            "</table>\n" +
            "    \n" +
            "<!--\n" +
            "        \n" +
            "    \n" +
            "-->\n" +
            "<br /><br />\n" +
            "<div id=\"petango_ad\" style=\"text-align:center\"><a href=\"http://www.petango.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" /></a></div>\n" +
            "<!-- Begin comScore Tag -->\n" +
            "<script>\n" +
            "    document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
            "</script>\n" +
            "\n" +
            "<script>\n" +
            "  COMSCORE.beacon({\n" +
            "    c1:2,\n" +
            "    c2:6745171,\n" +
            "    c3:\"\",\n" +
            "    c4:\"\",\n" +
            "    c5:\"\",\n" +
            "    c6:\"\",\n" +
            "    c15:\"\"\n" +
            "  });\n" +
            "</script>\n" +
            "<noscript>\n" +
            "  <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
            "</noscript>\n" +
            "<!-- End comScore Tag -->\n" +
            "\n" +
            "</div>\n" +
            "</body>\n" +
            "    </html>"

    static OREO_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href= type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Oreo\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/239232bf-182c-4149-9c1a-23f259e8cbf8.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"**Important Note** You won't find this kitty at the shelter. This kitty is waiting patiently in foster care for your call. Please contact the Foster Care Hotline at 206-684-0685 to arrange a meeting. Thank you! \n" +
        "\n" +
        "Oreo is a talkative chonkster who has earned her nickname of \\“Double Stuf\\“. Treats and regular meals are the keys to her heart. She keeps active with food puzzle toys. Her scratching posts must be sturdy and stable to withstand her powerful (if uncoordinated) assaults. Sacrifice mousies and feathers to her frequently to earn her benevolence.\n" +
        "\n" +
        "Oreo was quite anxious at the shelter, but gradually came out of her shell in foster care. Let her approach you and you'll be rewarded with head bonks and chirrups. She loves a good head scratch but is easily overstimulated, so don't be offended by a hiss - just lay off and she'll return when she's ready. She'd probably be best with experienced cat people with no small children, dogs, or other cats.\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=29127858\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:history.go(-1); \" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/returntolisting.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Oreo</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/239232bf-182c-4149-9c1a-23f259e8cbf8.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/239232bf-182c-4149-9c1a-23f259e8cbf8.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/239232bf-182c-4149-9c1a-23f259e8cbf8.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/e44d6521-7dbf-4295-9076-ac4999f8c942.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/e44d6521-7dbf-4295-9076-ac4999f8c942.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/34e87548-b035-4096-b765-3e0bea7c912e.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/34e87548-b035-4096-b765-3e0bea7c912e.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">29127858</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Cat</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">Domestic Shorthair</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\">4 years 7 months 3 days</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Female</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Medium</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Brown/White</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAltered\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Spayed/Neutered</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                &nbsp;<img id=\"ImageAltered\" src=\"images/GreenCheck.JPG\" style=\"height:15px;width:15px;\" />\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Foster Care</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">**Important Note** You won't find this kitty at the shelter. This kitty is waiting patiently in foster care for your call. Please contact the Foster Care Hotline at 206-684-0685 to arrange a meeting. Thank you! <br/><br/>Oreo is a talkative chonkster who has earned her nickname of \"Double Stuf\". Treats and regular meals are the keys to her heart. She keeps active with food puzzle toys. Her scratching posts must be sturdy and stable to withstand her powerful (if uncoordinated) assaults. Sacrifice mousies and feathers to her frequently to earn her benevolence.<br/><br/>Oreo was quite anxious at the shelter, but gradually came out of her shell in foster care. Let her approach you and you'll be rewarded with head bonks and chirrups. She loves a good head scratch but is easily overstimulated, so don't be offended by a hiss - just lay off and she'll return when she's ready. She'd probably be best with experienced cat people with no small children, dogs, or other cats.</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        <div id=\"tbl24PWtrial\">\n" +
        "\t\n" +
        "                                <div style=\"margin-left: auto; margin-right: auto; float: left; display:inline-block;\">\n" +
        "                                <a id=\"HyperLink2\" href=\"http://www.24PetWatch.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/24PW-Web-Services-Graphic-Trial.png\" border=\"0\" alt=\"\" /></a>\n" +
        "                                </div>\n" +
        "                        \n" +
        "</div>\n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"

    static HTML_DOGS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head>\n" +
        "\n" +
        "    <title>Adoptable Animals</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css\" type=\"text/css\" />\n" +
        "    <style type=\"text/css\">\n" +
        "        .hidden { display: none; }\n" +
        "    </style>\n" +
        "    <script language=javascript type=\"text/javascript\">\n" +
        "        var newwindow;\n" +
        "        function poptastic(url)\n" +
        "        {\n" +
        "\t        newwindow=window.open(url,'Animal_Details','height=700,width=700,scrollbars=1,resizable=1');\n" +
        "\t        if (window.focus) {newwindow.focus()}\n" +
        "        }\n" +
        "    </script>\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "\n" +
        "<body class=\"list-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "<div class=\"list-body\">\n" +
        "    <table id=\"tblSearchResults\" class=\"list-table\" cellspacing=\"2\" border=\"0\" style=\"width:100%\">\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29000199&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29000199&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Squid</a></div>\n" +
        "<div class=\"list-animal-id\">29000199</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, American Pit Bull/Mix</div>\n" +
        "<div class=\"list-animal-age\">12 years 4 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29032457&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/71e134b6-8e1b-45ec-bd30-b79fc6ab500a_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29032457&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Peanut</a></div>\n" +
        "<div class=\"list-animal-id\">29032457</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Chihuahua, Short Coat/Mix</div>\n" +
        "<div class=\"list-animal-age\">7 years</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29117708&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/a10c8e96-91d8-4760-8128-4e8136e1e74c_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29117708&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Elias (Eli) NEW INFO</a></div>\n" +
        "<div class=\"list-animal-id\">29117708</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Rottweiler</div>\n" +
        "<div class=\"list-animal-age\">5 years 6 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29125703&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/3fe11f3d-c6d2-47e1-bdbb-a7f10d9467d0_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29125703&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Rosa</a></div>\n" +
        "<div class=\"list-animal-id\">29125703</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, American Pit Bull</div>\n" +
        "<div class=\"list-animal-age\">8 years 3 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35117717&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/5e29535f-f012-4d04-bb8c-8f9e658ce05c_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35117717&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Cairo NEW INFO</a></div>\n" +
        "<div class=\"list-animal-id\">35117717</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, American Pit Bull/Mix</div>\n" +
        "<div class=\"list-animal-age\">3 years 10 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37767290&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37767290&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Buddy</a></div>\n" +
        "<div class=\"list-animal-id\">37767290</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Ibizan Hound/Mix</div>\n" +
        "<div class=\"list-animal-age\">2 years 1 month</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37795276&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/93593445-49a6-424a-b525-e73cb14a4515_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37795276&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Stella</a></div>\n" +
        "<div class=\"list-animal-id\">37795276</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, Pit Bull/Mix</div>\n" +
        "<div class=\"list-animal-age\">8 years</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37868239&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/ab290c9a-0356-4a13-aea9-2ba31b9caca6_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=37868239&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Emme</a></div>\n" +
        "<div class=\"list-animal-id\">37868239</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, Pit Bull/Mix</div>\n" +
        "<div class=\"list-animal-age\">8 years</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39183205&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/3bd88b79-9bfc-48b4-8728-003f401f9199_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39183205&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Ziggy NEW INFO</a></div>\n" +
        "<div class=\"list-animal-id\">39183205</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Great Dane</div>\n" +
        "<div class=\"list-animal-age\">2 years 2 months</div>\n" +
        "<div class=\"hidden\">Staff Office</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39424634&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/49a59d45-1db3-4637-96cc-8fec6d02fbcc_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39424634&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Leonitus Cornelius</a></div>\n" +
        "<div class=\"list-animal-id\">39424634</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Chihuahua, Short Coat/Mix</div>\n" +
        "<div class=\"list-animal-age\">9 years 6 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39424639&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e96e4215-5569-4d65-808a-f8ff3bc87fc9_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39424639&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Little Bella Renault</a></div>\n" +
        "<div class=\"list-animal-id\">39424639</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Chihuahua, Short Coat</div>\n" +
        "<div class=\"list-animal-age\">3 years 4 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39845255&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/31684c62-c4db-44c1-bb85-df55b0afb37f_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39845255&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Edina</a></div>\n" +
        "<div class=\"list-animal-id\">39845255</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Beagle/Mix</div>\n" +
        "<div class=\"list-animal-age\">8 years 4 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40205906&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/da9a6263-c5e9-4ea7-bfad-87553c9ab24e_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40205906&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Annie</a></div>\n" +
        "<div class=\"list-animal-id\">40205906</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, Pit Bull/Chinese Shar-Pei</div>\n" +
        "<div class=\"list-animal-age\">7 years 2 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40336679&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/aebc6110-1e0d-4e57-8a7e-a85e22f6fc70_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40336679&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Cash</a></div>\n" +
        "<div class=\"list-animal-id\">40336679</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Retriever/Mix</div>\n" +
        "<div class=\"list-animal-age\">4 years 9 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40583945&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e0865a3a-3099-4271-be99-680182371272_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40583945&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Malia</a></div>\n" +
        "<div class=\"list-animal-id\">40583945</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Chihuahua, Short Coat/Dachshund, Standard Smooth Haired</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40644117&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/b55d002f-0b6a-4603-95a5-944c0d52a271_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40644117&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Lulabelle</a></div>\n" +
        "<div class=\"list-animal-id\">40644117</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Chihuahua, Short Coat</div>\n" +
        "<div class=\"list-animal-age\">11 years 7 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40697680&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/a89324f4-ddf1-4a24-a9e7-8d4fe408cf5c_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40697680&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Bella</a></div>\n" +
        "<div class=\"list-animal-id\">40697680</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Terrier, American Staffordshire/Mix</div>\n" +
        "<div class=\"list-animal-age\">3 years 1 month</div>\n" +
        "<div class=\"hidden\">Dog Kennels</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40784791&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e7afa3d9-525a-43cd-8234-11ef057d773d_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40784791&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Abaco</a></div>\n" +
        "<div class=\"list-animal-id\">40784791</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Retriever/Mix</div>\n" +
        "<div class=\"list-animal-age\">3 years</div>\n" +
        "<div class=\"hidden\">Dog Kennels</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40822276&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/ff63535d-e469-4145-907f-4a826dc0ddf3_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40822276&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Morgan</a></div>\n" +
        "<div class=\"list-animal-id\">40822276</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Retriever/Australian Cattle Dog</div>\n" +
        "<div class=\"list-animal-age\"> 7 months</div>\n" +
        "<div class=\"hidden\">(Unspecified)</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40830131&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/6dc3476d-f9bb-476e-a7a5-acb5432cb360_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40830131&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Ellen</a></div>\n" +
        "<div class=\"list-animal-id\">40830131</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Retriever, Labrador/Terrier, American Pit Bull</div>\n" +
        "<div class=\"list-animal-age\">10 years</div>\n" +
        "<div class=\"hidden\">Dog Kennels</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40988336&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/97bfdd0e-9d8c-48c1-89b1-6b03202f63ab_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40988336&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Nala</a></div>\n" +
        "<div class=\"list-animal-id\">40988336</div>\n" +
        "<div class=\"list-anima-species\">Dog</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Welsh Corgi, Cardigan/Terrier, Jack Russell</div>\n" +
        "<div class=\"list-animal-age\">7 years</div>\n" +
        "<div class=\"hidden\">Dog Kennels</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "</table>\n" +
        "    \n" +
        "<!--\n" +
        "        \n" +
        "    \n" +
        "-->\n" +
        "<br /><br />\n" +
        "<div id=\"petango_ad\" style=\"text-align:center\"><a href=\"http://www.petango.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" /></a></div>\n" +
        "<!-- Begin comScore Tag -->\n" +
        "<script>\n" +
        "    document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "</script>\n" +
        "\n" +
        "<script>\n" +
        "  COMSCORE.beacon({\n" +
        "    c1:2,\n" +
        "    c2:6745171,\n" +
        "    c3:\"\",\n" +
        "    c4:\"\",\n" +
        "    c5:\"\",\n" +
        "    c6:\"\",\n" +
        "    c15:\"\"\n" +
        "  });\n" +
        "</script>\n" +
        "<noscript>\n" +
        "  <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "</noscript>\n" +
        "<!-- End comScore Tag -->\n" +
        "\n" +
        "</div>\n" +
        "</body>\n" +
        "    </html>"

    static String SQUID_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href= type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Squid\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"Squid is in a foster home, and is currently not at the shelter. Please find info below on how to adopt her, thanks.\n" +
        "\n" +
        "Squid is a busy, brown-eyed girl who's a great companion for leisurely walks. At 12 years young, this pretty brindle girl will thrive in a moderately active home with adults who already have dog experience. Squid knows \\“sit\\“ and is house-trained. With a guiding hand and a bit of patience, she's eager to learn more! She would love to be your only pet. Come meet this loving yet independent lady, and see if she's the one for you!\n" +
        "\n" +
        "HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. \n" +
        "\n" +
        "Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we're open ONLY for redeeming lost pets, from 2 pm - 4 pm.\n" +
        "\n" +
        "IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=29000199\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:history.go(-1); \" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/returntolisting.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Squid</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/6d84a5bb-4e21-437c-9939-f21911bf4ca8.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/b48a9d84-9786-4c0c-8c5c-5691ed657d39.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/b48a9d84-9786-4c0c-8c5c-5691ed657d39.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/174dc51a-abfc-42a4-8c6b-f5abc1f01782.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/174dc51a-abfc-42a4-8c6b-f5abc1f01782.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">29000199</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Dog</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">Terrier, American Pit Bull/Mix</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\">12 years 4 months 24 days</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Female</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Medium</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Brindle</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAltered\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Spayed/Neutered</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                &nbsp;<img id=\"ImageAltered\" src=\"images/GreenCheck.JPG\" style=\"height:15px;width:15px;\" />\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Foster Care</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">Squid is in a foster home, and is currently not at the shelter. Please find info below on how to adopt her, thanks.<br/><br/>Squid is a busy, brown-eyed girl who's a great companion for leisurely walks. At 12 years young, this pretty brindle girl will thrive in a moderately active home with adults who already have dog experience. Squid knows \"sit\" and is house-trained. With a guiding hand and a bit of patience, she's eager to learn more! She would love to be your only pet. Come meet this loving yet independent lady, and see if she's the one for you!<br/><br/>HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. <br/><br/>Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we're open ONLY for redeeming lost pets, from 2 pm - 4 pm.<br/><br/>IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        <div id=\"tbl24PWtrial\">\n" +
        "\t\n" +
        "                                <div style=\"margin-left: auto; margin-right: auto; float: left; display:inline-block;\">\n" +
        "                                <a id=\"HyperLink2\" href=\"http://www.24PetWatch.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/24PW-Web-Services-Graphic-Trial.png\" border=\"0\" alt=\"\" /></a>\n" +
        "                                </div>\n" +
        "                        \n" +
        "</div>\n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"

    static String BUDDY_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css\" type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Buddy\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"** CHECK OUT BUDDY'S NEW VIDEO HERE  https://youtu.be/s6j7024jHfw **\n" +
        "\n" +
        "Buddy is in a foster home and is currently not at the shelter. Please see info below on how to adopt him - thanks\n" +
        "\n" +
        "Dynamic Dog Seeks Forever Family:\n" +
        "\n" +
        "Me: Athletic and energetic, I love playing with other dogs. I'm a little over a year old, and a tall, lovable mix of Ibizan Hound and some other good stuff! I'm a great hiking/jogging/fast walking partner, and a real life, \\“talking\\“ doorbell: which translates to, I'll let you know when we have guests! At the end of a productive day, I want to lay in your lap! I'm shy with new people but get attached quickly. I'm a leggy 65 pounds or so.\n" +
        "\n" +
        "You: Can provide the active lifestyle I need, plenty of chew toys, and have experience using reward-based training techniques. I'd love a yard to play games with my dog sibling and/or friends. A committed, invested family is very important! Kids make me a little uneasy, but with slow introductions, I'd probably welcome a dog-savvy, feline sibling. (and did I mention I like dogs??)\n" +
        "\n" +
        "If you like what you've read about me, and \\“Match\\“ my needs, please send an application. Thanks!\n" +
        "\n" +
        " HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. \n" +
        "\n" +
        "Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we're open ONLY for redeeming lost pets, from 2 pm - 4 pm. \n" +
        "\n" +
        "IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=37767290&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:window.close();\" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/closethiswindow.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Buddy</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/4c0e60c1-a3ac-4ab4-b423-6440f54d5ecf.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/7ab24826-e644-42c6-ba35-c1d0da831c98.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/7ab24826-e644-42c6-ba35-c1d0da831c98.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/6b12b1a9-cfcd-4a01-b668-3cf6fb93738b.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/6b12b1a9-cfcd-4a01-b668-3cf6fb93738b.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\" onclick=\"loadVideo(&#39;s6j7024jHfw&#39;); return false;\" href=\"s6j7024jHfw\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">37767290</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Dog</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">Ibizan Hound/Mix</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\">2 years  1 month 4 days</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Male</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Large</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Red</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAltered\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Spayed/Neutered</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                &nbsp;<img id=\"ImageAltered\" src=\"images/GreenCheck.JPG\" style=\"height:15px;width:15px;\" />\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Foster Care</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">** CHECK OUT BUDDY'S NEW VIDEO HERE  https://youtu.be/s6j7024jHfw **<br/><br/>Buddy is in a foster home and is currently not at the shelter. Please see info below on how to adopt him - thanks<br/><br/>Dynamic Dog Seeks Forever Family:<br/><br/>Me: Athletic and energetic, I love playing with other dogs. I'm a little over a year old, and a tall, lovable mix of Ibizan Hound and some other good stuff! I'm a great hiking/jogging/fast walking partner, and a real life, \"talking\" doorbell: which translates to, I'll let you know when we have guests! At the end of a productive day, I want to lay in your lap! I'm shy with new people but get attached quickly. I'm a leggy 65 pounds or so.<br/><br/>You: Can provide the active lifestyle I need, plenty of chew toys, and have experience using reward-based training techniques. I'd love a yard to play games with my dog sibling and/or friends. A committed, invested family is very important! Kids make me a little uneasy, but with slow introductions, I'd probably welcome a dog-savvy, feline sibling. (and did I mention I like dogs??)<br/><br/>If you like what you've read about me, and \"Match\" my needs, please send an application. Thanks!<br/><br/> HOW TO ADOPT - Please fill out a Dog Adoption Application, available at http://www.seattle.gov/animalshelter/forms.htm and email it to the Seattle Animal Shelter Foster Dog Program at adoptionreview@gmail.com. <br/><br/>Alternatively, you may fill out or drop off the application at the Seattle Animal Shelter, located at 2061 15th Ave. W (1 mile south of the Ballard Bridge, at the corner of W. Armory Way). The shelter is open five days a week (Wednesday - Sunday), from 1 pm - 6 pm. Mondays and Tuesdays we're open ONLY for redeeming lost pets, from 2 pm - 4 pm. <br/><br/>IMPORTANT NOTE: Many of our dogs will receive numerous applications, and the Seattle Animal Shelter asks for your patience during the process of finding the very best match. If you have questions about this dog that have not been answered above, please contact us at sasfosterdogs@gmail.com. If you would like to arrange to meet this dog, you must submit an adoption application. Thank you!</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        <div id=\"tbl24PWtrial\">\n" +
        "\t\n" +
        "                                <div style=\"margin-left: auto; margin-right: auto; float: left; display:inline-block;\">\n" +
        "                                <a id=\"HyperLink2\" href=\"http://www.24PetWatch.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/24PW-Web-Services-Graphic-Trial.png\" border=\"0\" alt=\"\" /></a>\n" +
        "                                </div>\n" +
        "                        \n" +
        "</div>\n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"

    static HTML_RABBITS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head>\n" +
        "\n" +
        "    <title>Adoptable Animals</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css\" type=\"text/css\" />\n" +
        "    <style type=\"text/css\">\n" +
        "        .hidden { display: none; }\n" +
        "    </style>\n" +
        "    <script language=javascript type=\"text/javascript\">\n" +
        "        var newwindow;\n" +
        "        function poptastic(url)\n" +
        "        {\n" +
        "\t        newwindow=window.open(url,'Animal_Details','height=700,width=700,scrollbars=1,resizable=1');\n" +
        "\t        if (window.focus) {newwindow.focus()}\n" +
        "        }\n" +
        "    </script>\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "\n" +
        "<body class=\"list-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "<div class=\"list-body\">\n" +
        "    <table id=\"tblSearchResults\" class=\"list-table\" cellspacing=\"2\" border=\"0\" style=\"width:100%\">\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35407806&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2bfaa63e-0013-4929-8c35-f4c1df0fb140_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=35407806&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Zoey</a></div>\n" +
        "<div class=\"list-animal-id\">35407806</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">American/American</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39402273&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/69bb653e-2490-4a6b-994c-7a927f8bdb84_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39402273&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Bonny</a></div>\n" +
        "<div class=\"list-animal-id\">39402273</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Mini Rex/American</div>\n" +
        "<div class=\"list-animal-age\">5 years</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39812570&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e4d7b576-ce0a-4bda-af62-d53a0e2750ca_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39812570&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Teddy</a></div>\n" +
        "<div class=\"list-animal-id\">39812570</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Lionhead/American</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40345564&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/3bc0d6b6-85f8-46ea-a99f-97ee637c551f_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40345564&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Serena</a></div>\n" +
        "<div class=\"list-animal-id\">40345564</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">American/American</div>\n" +
        "<div class=\"list-animal-age\">2 years 3 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40473897&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/7b9bba7b-c38f-4cf8-bdfa-e2e93885bf39_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40473897&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Joey</a></div>\n" +
        "<div class=\"list-animal-id\">40473897</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Dutch/American</div>\n" +
        "<div class=\"list-animal-age\"> 6 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40499719&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/3f7650a5-2d7e-4061-b335-963567662685_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40499719&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Pixie</a></div>\n" +
        "<div class=\"list-animal-id\">40499719</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Dutch/American</div>\n" +
        "<div class=\"list-animal-age\"> 5 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40592751&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/4b1b4f00-4e95-4d39-bdc2-b8c58e240714_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40592751&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Nia</a></div>\n" +
        "<div class=\"list-animal-id\">40592751</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Lionhead/American</div>\n" +
        "<div class=\"list-animal-age\">4 years 1 month</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40669707&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/f386b233-b6ea-478d-bd0a-04fe670204ee_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40669707&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Judy Hopps</a></div>\n" +
        "<div class=\"list-animal-id\">40669707</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">American</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40698062&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/f28b4b58-5039-47ae-af5b-786054cfd231_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40698062&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Freya</a></div>\n" +
        "<div class=\"list-animal-id\">40698062</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Lionhead/Lionhead</div>\n" +
        "<div class=\"list-animal-age\">2 years 2 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40725140&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/b5ee87ae-5b55-4af8-8a73-e87a609c082f_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40725140&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Hennessy</a></div>\n" +
        "<div class=\"list-animal-id\">40725140</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">American/American</div>\n" +
        "<div class=\"list-animal-age\">2 years 1 month</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40839553&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/3d8a2ba0-9719-4b4b-863a-293806ae0752_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40839553&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Daikon</a></div>\n" +
        "<div class=\"list-animal-id\">40839553</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Male/Neutered</div>\n" +
        "<div class=\"list-animal-breed\">Holland Lop</div>\n" +
        "<div class=\"list-animal-age\">3 years</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40839555&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40839555&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true');\" >Tofu</a></div>\n" +
        "<div class=\"list-animal-id\">40839555</div>\n" +
        "<div class=\"list-anima-species\">Rabbit</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">American</div>\n" +
        "<div class=\"list-animal-age\">3 years</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "</table>\n" +
        "    \n" +
        "<!--\n" +
        "        \n" +
        "    \n" +
        "-->\n" +
        "<br /><br />\n" +
        "<div id=\"petango_ad\" style=\"text-align:center\"><a href=\"http://www.petango.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" /></a></div>\n" +
        "<!-- Begin comScore Tag -->\n" +
        "<script>\n" +
        "    document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "</script>\n" +
        "\n" +
        "<script>\n" +
        "  COMSCORE.beacon({\n" +
        "    c1:2,\n" +
        "    c2:6745171,\n" +
        "    c3:\"\",\n" +
        "    c4:\"\",\n" +
        "    c5:\"\",\n" +
        "    c6:\"\",\n" +
        "    c15:\"\"\n" +
        "  });\n" +
        "</script>\n" +
        "<noscript>\n" +
        "  <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "</noscript>\n" +
        "<!-- End comScore Tag -->\n" +
        "\n" +
        "</div>\n" +
        "</body>\n" +
        "    </html>"

    static String TOFU_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css\" type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Tofu\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"Friendly, curious, and fluffy! That's Daikon and Tofu!\n" +
        "\n" +
        "Daikon and Tofu are Holland Lop adult rabbits who are fully grown three-year-old siblings. Daikon is the male grey rabbit and his sister Tofu is the brown rabbit. Tofu and Daikon are a bonded pair and will be adopted together.  \n" +
        "\n" +
        "They both have beautiful fur; Daikon's is short and velvety soft to the touch and Tofu's is fluffier and super soft. These are two healthy siblings have been microchipped and spayed and are ready to become part of your family. \n" +
        "\n" +
        "After an afternoon of playtime, Daikon and Tofu wanted you to know that they like to hop around and stretch their legs and may even lounge with/on you while watching TV. Their temperament seems to be a bit shy at first but eventually becoming confident and curious. They get along well with people and each other and tolerate petting and some handling. They have both lived in a home with cats. \n" +
        "\n" +
        "Daikon and Tofu are looking forward to Springing Ahead! To meet you and going home to your loving forever home.  \n" +
        "\n" +
        "\n" +
        "HOW TO ADOPT: \n" +
        "Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week, Wednesday-Sunday, 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays.\n" +
        "\n" +
        "HOUSE RABBIT CARE:\n" +
        "Housing requirements for house rabbits should be a metal exercise pen set up in a formation that allows the rabbit to hop a few times in each direction (at least 8 square feet for a single rabbit). Depending on how high the rabbit can jump, the pen may need to be 3 feet to 4 feet tall. Your rabbit's enclosure should include at least one litter box with fresh hay, food and water bowls, and a refuge where the rabbit can get out of sight, such as a cardboard box with multiple holes cut out work, soft places to lie down and toys for enrichment. \n" +
        "\n" +
        "Your rabbit should get at least one to two hours daily outside its pen in a larger space (at least 24 square feet). Rabbits are healthiest when they have lots of space to run, jump, play and interact with you. Make sure any room your rabbit visits is \\“bunny-proofed.\\“ Hide or remove electrical wires and remove toxic house plants.\n" +
        "\n" +
        "Rabbits are intelligent and social animals; they do best with a rabbit companion and need daily interaction with their human friends. Rabbits should be fed unlimited hay, high-quality hay pellets, and fresh leafy greens. (Leaf lettuce, carrot tops, parsley, cilantro, and pesticide-free dandelion are good choices.) Treats include small portions of carrot or fruit, such as apple, berries, pear, grape, or banana.\n" +
        "\n" +
        "You are expected to make a lifetime commitment to your rabbit, and they should be treated as an integral part of the family. The primary caregiver must be a responsible adult, and young children should always be supervised when interacting with your rabbit and instructed on proper approach and handling. Household cats or dogs should, of course, also be carefully supervised at all times and introduced in a calm and quiet manner.\n" +
        "Your rabbit will need regular nail trims and annual vet checks. With proper care and lots of love, rabbits can live to be 10-12 years old.\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=40839555&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&PopUp=true\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:window.close();\" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/closethiswindow.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Tofu</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/bac45529-eed7-4f35-b934-780754d59669.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/b31bf1f8-04bf-404c-8ec9-b17dc509b6c9.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/b31bf1f8-04bf-404c-8ec9-b17dc509b6c9.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/55623bc9-39d2-4456-a496-ffa63ada1a85.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/55623bc9-39d2-4456-a496-ffa63ada1a85.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">40839555</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Rabbit</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">American</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\">3 years 15 days</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Female</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Medium</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Brown</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAltered\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Spayed/Neutered</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                &nbsp;<img id=\"ImageAltered\" src=\"images/GreenCheck.JPG\" style=\"height:15px;width:15px;\" />\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Critter Room</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">Friendly, curious, and fluffy! That's Daikon and Tofu!<br/><br/>Daikon and Tofu are Holland Lop adult rabbits who are fully grown three-year-old siblings. Daikon is the male grey rabbit and his sister Tofu is the brown rabbit. Tofu and Daikon are a bonded pair and will be adopted together.  <br/><br/>They both have beautiful fur; Daikon's is short and velvety soft to the touch and Tofu's is fluffier and super soft. These are two healthy siblings have been microchipped and spayed and are ready to become part of your family. <br/><br/>After an afternoon of playtime, Daikon and Tofu wanted you to know that they like to hop around and stretch their legs and may even lounge with/on you while watching TV. Their temperament seems to be a bit shy at first but eventually becoming confident and curious. They get along well with people and each other and tolerate petting and some handling. They have both lived in a home with cats. <br/><br/>Daikon and Tofu are looking forward to Springing Ahead! To meet you and going home to your loving forever home.  <br/><br/><br/>HOW TO ADOPT: <br/>Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week, Wednesday-Sunday, 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays.<br/><br/>HOUSE RABBIT CARE:<br/>Housing requirements for house rabbits should be a metal exercise pen set up in a formation that allows the rabbit to hop a few times in each direction (at least 8 square feet for a single rabbit). Depending on how high the rabbit can jump, the pen may need to be 3 feet to 4 feet tall. Your rabbit's enclosure should include at least one litter box with fresh hay, food and water bowls, and a refuge where the rabbit can get out of sight, such as a cardboard box with multiple holes cut out work, soft places to lie down and toys for enrichment. <br/><br/>Your rabbit should get at least one to two hours daily outside its pen in a larger space (at least 24 square feet). Rabbits are healthiest when they have lots of space to run, jump, play and interact with you. Make sure any room your rabbit visits is \"bunny-proofed.\" Hide or remove electrical wires and remove toxic house plants.<br/><br/>Rabbits are intelligent and social animals; they do best with a rabbit companion and need daily interaction with their human friends. Rabbits should be fed unlimited hay, high-quality hay pellets, and fresh leafy greens. (Leaf lettuce, carrot tops, parsley, cilantro, and pesticide-free dandelion are good choices.) Treats include small portions of carrot or fruit, such as apple, berries, pear, grape, or banana.<br/><br/>You are expected to make a lifetime commitment to your rabbit, and they should be treated as an integral part of the family. The primary caregiver must be a responsible adult, and young children should always be supervised when interacting with your rabbit and instructed on proper approach and handling. Household cats or dogs should, of course, also be carefully supervised at all times and introduced in a calm and quiet manner.<br/>Your rabbit will need regular nail trims and annual vet checks. With proper care and lots of love, rabbits can live to be 10-12 years old.</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"

    static String HTML_SMALL_MAMMALS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head>\n" +
        "\n" +
        "    <title>Adoptable Animals</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css\" type=\"text/css\" />\n" +
        "    <style type=\"text/css\">\n" +
        "        .hidden { display: none; }\n" +
        "    </style>\n" +
        "    <script language=javascript type=\"text/javascript\">\n" +
        "        var newwindow;\n" +
        "        function poptastic(url)\n" +
        "        {\n" +
        "\t        newwindow=window.open(url,'Animal_Details','height=700,width=700,scrollbars=1,resizable=1');\n" +
        "\t        if (window.focus) {newwindow.focus()}\n" +
        "        }\n" +
        "    </script>\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "\n" +
        "<body class=\"list-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "<div class=\"list-body\">\n" +
        "    <table id=\"tblSearchResults\" class=\"list-table\" cellspacing=\"2\" border=\"0\" style=\"width:100%\">\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29555668&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2f965f07-9c6a-4395-ac1e-575e193c8107_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29555668&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Zoe</a></div>\n" +
        "<div class=\"list-animal-id\">29555668</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Chinchilla</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29555786&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/90b24fb3-5c23-4878-b6e4-046315371c2a_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=29555786&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Abby</a></div>\n" +
        "<div class=\"list-animal-id\">29555786</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Chinchilla</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415293&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415293&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Daenerys</a></div>\n" +
        "<div class=\"list-animal-id\">40415293</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415297&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/4299c066-f85d-49a4-9714-a13fce88cf2f_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415297&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Olenna</a></div>\n" +
        "<div class=\"list-animal-id\">40415297</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415306&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/7ba8136f-9e99-4267-b4cc-1e53ceeec569_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415306&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Meera</a></div>\n" +
        "<div class=\"list-animal-id\">40415306</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415309&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/87a16b03-7bac-424f-8774-69db6512f520_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415309&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Brienne</a></div>\n" +
        "<div class=\"list-animal-id\">40415309</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415312&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2448dc15-b64b-42cb-b9a1-2903b24c7258_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415312&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Ygritte</a></div>\n" +
        "<div class=\"list-animal-id\">40415312</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415314&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2ec37214-926b-4e94-b4ba-5fe92c091e51_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415314&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Margaery</a></div>\n" +
        "<div class=\"list-animal-id\">40415314</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415538&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/d04e8951-9ef9-45f0-bab8-3479b6713714_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40415538&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Yara</a></div>\n" +
        "<div class=\"list-animal-id\">40415538</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Hamster/Hamster</div>\n" +
        "<div class=\"list-animal-age\"> 8 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684195&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e1dbeba1-2528-4cb5-bb11-dd8a54c9b7fe_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684195&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Abigail</a></div>\n" +
        "<div class=\"list-animal-id\">40684195</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"> 2 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684247&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/d5659cf5-260c-413c-aa52-19d002264e85_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684247&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Xena</a></div>\n" +
        "<div class=\"list-animal-id\">40684247</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"> 2 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684270&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/61600d9f-3cc9-4953-82c1-13d1a47fb13d_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684270&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Winonna</a></div>\n" +
        "<div class=\"list-animal-id\">40684270</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"> 2 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684302&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/a1f281b6-8c35-4581-a756-9e7f50e44332_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684302&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Winter</a></div>\n" +
        "<div class=\"list-animal-id\">40684302</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684750&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/0970b3e4-cbd8-464a-935e-9855eb6e0ae2_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684750&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Veronica</a></div>\n" +
        "<div class=\"list-animal-id\">40684750</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684756&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/cd87b0c9-c596-4eb3-a031-08204c03aae8_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684756&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Tamsin</a></div>\n" +
        "<div class=\"list-animal-id\">40684756</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\">1 year 1 month</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684762&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2f224fa5-87f8-4e4c-9cb9-4ca1a746f92b_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40684762&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Sally</a></div>\n" +
        "<div class=\"list-animal-id\">40684762</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Mouse/Mouse</div>\n" +
        "<div class=\"list-animal-age\"> 2 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40739537&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/634d59dd-6e21-4ac8-ba8c-1ada83b6e12e_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40739537&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Book</a></div>\n" +
        "<div class=\"list-animal-id\">40739537</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Rat/Rat</div>\n" +
        "<div class=\"list-animal-age\">1 year 7 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40739546&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/d28bcf91-6da8-426b-adc2-abed30196b1d_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40739546&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Biscuit</a></div>\n" +
        "<div class=\"list-animal-id\">40739546</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Rat/Rat</div>\n" +
        "<div class=\"list-animal-age\">1 year 7 months</div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=41037716&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/31dcc672-bce6-4335-8905-7ffcaea84cfb_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=41037716&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Debra</a></div>\n" +
        "<div class=\"list-animal-id\">41037716</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female/Spayed</div>\n" +
        "<div class=\"list-animal-breed\">Guinea Pig/Guinea Pig</div>\n" +
        "<div class=\"list-animal-age\"> 11 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=41037722&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/e638ce37-0c7a-4caa-a4a9-584414818c28_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=41037722&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Popcorn</a></div>\n" +
        "<div class=\"list-animal-id\">41037722</div>\n" +
        "<div class=\"list-anima-species\">Small&Furry</div>\n" +
        "<div class=\"list-animal-sexSN\">Female</div>\n" +
        "<div class=\"list-animal-breed\">Guinea Pig/Guinea Pig</div>\n" +
        "<div class=\"list-animal-age\"> 11 months</div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "</table>\n" +
        "    \n" +
        "<!--\n" +
        "        \n" +
        "    \n" +
        "-->\n" +
        "<br /><br />\n" +
        "<div id=\"petango_ad\" style=\"text-align:center\"><a href=\"http://www.petango.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" /></a></div>\n" +
        "<!-- Begin comScore Tag -->\n" +
        "<script>\n" +
        "    document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "</script>\n" +
        "\n" +
        "<script>\n" +
        "  COMSCORE.beacon({\n" +
        "    c1:2,\n" +
        "    c2:6745171,\n" +
        "    c3:\"\",\n" +
        "    c4:\"\",\n" +
        "    c5:\"\",\n" +
        "    c6:\"\",\n" +
        "    c15:\"\"\n" +
        "  });\n" +
        "</script>\n" +
        "<noscript>\n" +
        "  <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "</noscript>\n" +
        "<!-- End comScore Tag -->\n" +
        "\n" +
        "</div>\n" +
        "</body>\n" +
        "    </html>"

    static String DAENERYS_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css\" type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Daenerys\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"Meet Daenerys Hamsterborn! Don't worry about dragons with this lady. They are tamed and well-hidden within her carefresh. You'll never know they are around!\n" +
        "\n" +
        "Daenerys is a happy 7-month-old Black Bear hamster who is friendly, easy to handle, loves running on her wheel, and is looking for a new furever home! \n" +
        "\n" +
        "Come meet Daenerys at the Seattle Animal Shelter Critter room Wed-Sun 1-6pm!\n" +
        "\n" +
        "HOW TO ADOPT: Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays.\n" +
        "\n" +
        "HAMSTER CARE:\n" +
        "HOUSING: Adult hamsters need to live alone, never house them with another hamster. You'll need to house your hamster in a cage or a tank.  Glass tanks are preferable because they are safer than cages. The minimum recommended size for a Syrian or Teddy Bear hamster is 24 inches by 12 inches, and 12 inches high. If you are getting a cage, ensure the bars lining the cage aren't too big for your dwarf hamster to squeeze through. \n" +
        "\n" +
        "Make sure that you have a suitable location to place your hamster's new home. The location should be away from direct sunlight or draft as well as quiet and safe from potential predators (including other pets such as cats and dogs). Do note that your dwarf hamster is nocturnal and will be squeaking, scratching, or using the wheel a lot at night, so your bedroom might not be the best place. \n" +
        "\n" +
        "The tank or cage bottom should be covered with bedding that is non-toxic and comfortable for your new hamster. Paper bedding is recommended. The bedding should be around 2-3 inches in depth and your hamster will move it around as they see fit while they are nesting. Cedar and pine shavings should not be used as they may cause your pet to develop respiratory problems such as asthma. \n" +
        "\n" +
        "You will need to remove all the bedding, old food, and completely clean the tank every week. Also, clean the wheel and any toys too.\n" +
        "It is important that you get a solid plastic wheel, 9'' or 12'' in diameter to run on. Do not use metal wheels with openings as these can cause injuries. A \\“saucer\\“ wheel also works well for hamsters. Other things you will need in the cage are a hanging water bottle, small food dish, a house or other hiding place, some fresh hay, and chew blocks or toys.  \n" +
        "\n" +
        "EXERCISE: Hamsters have tons of energy and need to run on their wheel every day. Make certain that the wheel is able to spin freely, as it sometimes gets stuck into the bedding. If the wheel can't move this can stress or harm the hamster.\n" +
        "Consider getting a \\“tank topper\\“ for your glass tank so you can add ramps, extra hiding places and more room to explore in. Plastic hamster balls are also a fun way to exercise your hamster. They should be 12 inches in diameter and limit their time inside to about 5-10 minutes.\n" +
        "\n" +
        "HANDLING: Start by talking gently to your hamster so it becomes used to your voice. Introduce your hand into the cage with a treat and allow your hamster to approach you. Remember hamsters are easily startled. Take extra care when wakening them up, as they may get scared and nip at you. Lift handle your hamster gently. Scoop them up in both hands and hold them with cupped hands so they'll feel safe and secure. Keep your hands over a table or close to the floor in case your hamster drops out! \n" +
        "\n" +
        "FOOD: It is recommended that you feed your hamster a pelleted hamster food. Pelleted foods will give your hamster optimal nutrition because pellets contain a healthy blend of grains, seeds, vegetables, fruits, proteins, vitamins, and minerals. A small handful of fresh grass hay is recommended. Treats should be fed very sparingly. Use sugar-free treats like rolled oats or cereal \\“O's\\“.  Small amounts of leafy greens, dandelion, lettuce, cilantro are good choices. A couple of pieces of corn or peas are great treats too. Limit fruit to very small pieces, a few times a week, about the size of a blueberry, or half of a raspberry.\n" +
        "\n" +
        "A Syrian hamster lifespan 2 to 3 years. The adults grow to about 5 to 7 inches long and should weigh about 5 to 7 ounces.\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=40415293&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:window.close();\" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/closethiswindow.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Daenerys</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/470cd0d1-8ae0-43da-bd41-e10dcb2e2dcb.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/b1c6d810-ed92-4a16-84e4-1737ee4b8773.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/b1c6d810-ed92-4a16-84e4-1737ee4b8773.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/8a37ec67-5e29-47fd-b8e8-5f3f813ca750.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/8a37ec67-5e29-47fd-b8e8-5f3f813ca750.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">40415293</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Small&Furry</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">Hamster/Hamster</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\">8 months 20 days</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Female</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Large</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Black</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Critter Room</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">Meet Daenerys Hamsterborn! Don't worry about dragons with this lady. They are tamed and well-hidden within her carefresh. You'll never know they are around!<br/><br/>Daenerys is a happy 7-month-old Black Bear hamster who is friendly, easy to handle, loves running on her wheel, and is looking for a new furever home! <br/><br/>Come meet Daenerys at the Seattle Animal Shelter Critter room Wed-Sun 1-6pm!<br/><br/>HOW TO ADOPT: Come to the Shelter and fill out your critter adoption paperwork and then meet with an Animal Care Officer to speak with them about adopting. The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and all legal holidays.<br/><br/>HAMSTER CARE:<br/>HOUSING: Adult hamsters need to live alone, never house them with another hamster. You'll need to house your hamster in a cage or a tank.  Glass tanks are preferable because they are safer than cages. The minimum recommended size for a Syrian or Teddy Bear hamster is 24 inches by 12 inches, and 12 inches high. If you are getting a cage, ensure the bars lining the cage aren't too big for your dwarf hamster to squeeze through. <br/><br/>Make sure that you have a suitable location to place your hamster's new home. The location should be away from direct sunlight or draft as well as quiet and safe from potential predators (including other pets such as cats and dogs). Do note that your dwarf hamster is nocturnal and will be squeaking, scratching, or using the wheel a lot at night, so your bedroom might not be the best place. <br/><br/>The tank or cage bottom should be covered with bedding that is non-toxic and comfortable for your new hamster. Paper bedding is recommended. The bedding should be around 2-3 inches in depth and your hamster will move it around as they see fit while they are nesting. Cedar and pine shavings should not be used as they may cause your pet to develop respiratory problems such as asthma. <br/><br/>You will need to remove all the bedding, old food, and completely clean the tank every week. Also, clean the wheel and any toys too.<br/>It is important that you get a solid plastic wheel, 9'' or 12'' in diameter to run on. Do not use metal wheels with openings as these can cause injuries. A \"saucer\" wheel also works well for hamsters. Other things you will need in the cage are a hanging water bottle, small food dish, a house or other hiding place, some fresh hay, and chew blocks or toys.  <br/><br/>EXERCISE: Hamsters have tons of energy and need to run on their wheel every day. Make certain that the wheel is able to spin freely, as it sometimes gets stuck into the bedding. If the wheel can't move this can stress or harm the hamster.<br/>Consider getting a \"tank topper\" for your glass tank so you can add ramps, extra hiding places and more room to explore in. Plastic hamster balls are also a fun way to exercise your hamster. They should be 12 inches in diameter and limit their time inside to about 5-10 minutes.<br/><br/>HANDLING: Start by talking gently to your hamster so it becomes used to your voice. Introduce your hand into the cage with a treat and allow your hamster to approach you. Remember hamsters are easily startled. Take extra care when wakening them up, as they may get scared and nip at you. Lift handle your hamster gently. Scoop them up in both hands and hold them with cupped hands so they'll feel safe and secure. Keep your hands over a table or close to the floor in case your hamster drops out! <br/><br/>FOOD: It is recommended that you feed your hamster a pelleted hamster food. Pelleted foods will give your hamster optimal nutrition because pellets contain a healthy blend of grains, seeds, vegetables, fruits, proteins, vitamins, and minerals. A small handful of fresh grass hay is recommended. Treats should be fed very sparingly. Use sugar-free treats like rolled oats or cereal \"O's\".  Small amounts of leafy greens, dandelion, lettuce, cilantro are good choices. A couple of pieces of corn or peas are great treats too. Limit fruit to very small pieces, a few times a week, about the size of a blueberry, or half of a raspberry.<br/><br/>A Syrian hamster lifespan 2 to 3 years. The adults grow to about 5 to 7 inches long and should weigh about 5 to 7 ounces.</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"

    static String HTML_REPTILES = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head>\n" +
        "\n" +
        "    <title>Adoptable Animals</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css\" type=\"text/css\" />\n" +
        "    <style type=\"text/css\">\n" +
        "        .hidden { display: none; }\n" +
        "    </style>\n" +
        "    <script language=javascript type=\"text/javascript\">\n" +
        "        var newwindow;\n" +
        "        function poptastic(url)\n" +
        "        {\n" +
        "\t        newwindow=window.open(url,'Animal_Details','height=700,width=700,scrollbars=1,resizable=1');\n" +
        "\t        if (window.focus) {newwindow.focus()}\n" +
        "        }\n" +
        "    </script>\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "\n" +
        "<body class=\"list-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "<div class=\"list-body\">\n" +
        "    <table id=\"tblSearchResults\" class=\"list-table\" cellspacing=\"2\" border=\"0\" style=\"width:100%\">\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39763006&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=39763006&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Puff</a></div>\n" +
        "<div class=\"list-animal-id\">39763006</div>\n" +
        "<div class=\"list-anima-species\">Reptile</div>\n" +
        "<div class=\"list-animal-sexSN\">Male</div>\n" +
        "<div class=\"list-animal-breed\">Lizard</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Foster Care</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "<tr>\n" +
        "<td class=\"list-item\">\n" +
        "<div class=\"list-animal-photo-block\">\n" +
        "<a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40693268&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" ><img class=\"list-animal-photo\" src=\"//g.petango.com/photos/2681/dded8c9e-a45e-4053-96e2-11e763fe5598_TN1.jpg\" alt=\"Photo\"></a>\n" +
        "</div>\n" +
        "<div class=\"list-animal-info-block\">\n" +
        "<div class=\"list-animal-name\"><a href=\"javascript:poptastic('wsAdoptableAnimalDetails.aspx?id=40693268&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true');\" >Granite</a></div>\n" +
        "<div class=\"list-animal-id\">40693268</div>\n" +
        "<div class=\"list-anima-species\">Reptile</div>\n" +
        "<div class=\"list-animal-sexSN\">Male</div>\n" +
        "<div class=\"list-animal-breed\">Turtle/Turtle</div>\n" +
        "<div class=\"list-animal-age\"></div>\n" +
        "<div class=\"hidden\">Critter Room</div>\n" +
        "</div>\n" +
        "</td>\n" +
        "</tr>\n" +
        "</table>\n" +
        "    \n" +
        "<!--\n" +
        "        \n" +
        "    \n" +
        "-->\n" +
        "<br /><br />\n" +
        "<div id=\"petango_ad\" style=\"text-align:center\"><a href=\"http://www.petango.com\" target=\"_blank\"><img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" /></a></div>\n" +
        "<!-- Begin comScore Tag -->\n" +
        "<script>\n" +
        "    document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "</script>\n" +
        "\n" +
        "<script>\n" +
        "  COMSCORE.beacon({\n" +
        "    c1:2,\n" +
        "    c2:6745171,\n" +
        "    c3:\"\",\n" +
        "    c4:\"\",\n" +
        "    c5:\"\",\n" +
        "    c6:\"\",\n" +
        "    c15:\"\"\n" +
        "  });\n" +
        "</script>\n" +
        "<noscript>\n" +
        "  <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "</noscript>\n" +
        "<!-- End comScore Tag -->\n" +
        "\n" +
        "</div>\n" +
        "</body>\n" +
        "    </html>"

    static String PUFF_DETAILS = "\n" +
        "\n" +
        "\n" +
        "<!DOCTYPE html PUBLIC \"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">\n" +
        "<html xmlns=\"http://www.w3.org/1999/xhtml\">\n" +
        "<head><title>\n" +
        "\tAnimal Details\n" +
        "</title>\n" +
        "    <link id=\"stylesheet\" rel=\"stylesheet\" href=\"//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css\" type=\"text/css\" />\n" +
        "    <style type='text/css'>\n" +
        "        .logo\n" +
        "        { \n" +
        "            width: 185px !important;\n" +
        "            height: 57px !important;\n" +
        "        }\n" +
        "        .centeredImage\n" +
        "        {\n" +
        "            display: block;\n" +
        "            margin-left: auto;\n" +
        "            margin-right: auto;\n" +
        "        }\n" +
        "    </style>\n" +
        "\n" +
        "    <script type=\"text/javascript\">\n" +
        "        function loadPhoto(url) {\n" +
        "            document.imgAnimalPhoto.src = url;\n" +
        "        }\n" +
        "        function loadVideo(videoid) {\n" +
        "            window.open('wsYouTubeVideo.aspx?videoid=' + videoid, 'Video', 'status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=380');\n" +
        "        }\n" +
        "    </script>\n" +
        "    \n" +
        "\t<meta property=\"og:title\" content=\"Meet Puff\" />\n" +
        "\t<meta property=\"og:image\" content=\"http://g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8.jpg\" />\n" +
        "\t<meta property=\"og:description\" content=\"Puff is an amazing Bearded Dragon and he is looking for a great new forever home! Puff is very shy and a little bit unsocialized so he will need a lot of patience and time to get him to be the wonderful beardie that he is! Because of his special needs, he will have to be adopted by an experienced beardie family!\n" +
        "\n" +
        "Puff is currently being adored in foster care!\n" +
        "\n" +
        "HOW TO ADOPT: For foster animals, please fill out a critter adoption form available at the shelter. Then you must meet with an animal care officer IN PERSON at the shelter to review your application. Then we will arrange a day and time when the foster parent and critter will meet you at the Shelter critter room.\n" +
        "\n" +
        "The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and legal holidays. After the review, please allow 2-3 business days for the foster parent to reach out to you to set up a meet and greet.\" />\n" +
        "\t<meta property=\"og:url\" content=\"http://wspetangoprd2.azurewebsites.net/webservices/adoptablesearch/wsAdoptableAnimalDetails.aspx?id=39763006&css=http://www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles-breed.css&PopUp=true\" />\n" +
        "<script type=\"text/javascript\">var appInsights=window.appInsights||function(config){function t(config){i[config]=function(){var t=arguments;i.queue.push(function(){i[config].apply(i,t)})}}var i={config:config},u=document,e=window,o=\"script\",s=\"AuthenticatedUserContext\",h=\"start\",c=\"stop\",l=\"Track\",a=l+\"Event\",v=l+\"Page\",r,f;setTimeout(function(){var t=u.createElement(o);t.src=config.url||\"https://az416426.vo.msecnd.net/scripts/a/ai.0.js\";u.getElementsByTagName(o)[0].parentNode.appendChild(t)});try{i.cookie=u.cookie}catch(y){}for(i.queue=[],r=[\"Event\",\"Exception\",\"Metric\",\"PageView\",\"Trace\",\"Dependency\"];r.length;)t(\"track\"+r.pop());return t(\"set\"+s),t(\"clear\"+s),t(h+a),t(c+a),t(h+v),t(c+v),t(\"flush\"),config.disableExceptionTracking||(r=\"onerror\",t(\"_\"+r),f=e[r],e[r]=function(config,t,u,e,o){var s=f&&f(config,t,u,e,o);return s!==!0&&i[\"_\"+r](config,t,u,e,o),s}),i}({instrumentationKey:\"f4912ecb-fa06-49f4-91b5-98f64dcc9b55\",sdkExtension:\"a\"});window.appInsights=appInsights;appInsights.queue&&appInsights.queue.length===0&&appInsights.trackPageView();</script></head>\n" +
        "<body class=\"detail-body\">\n" +
        "        <!-- Google Tag Manager -->\n" +
        "    <noscript><iframe src=\"//www.googletagmanager.com/ns.html?id=GTM-55MWRN\"\n" +
        "height=\"0\" width=\"0\" style=\"display:none;visibility:hidden\"></iframe></noscript>\n" +
        "    <script>(function (w, d, s, l, i) {\n" +
        "    w[l] = w[l] || []; w[l].push({\n" +
        "        'gtm.start':\n" +
        "\n" +
        "        new Date().getTime(), event: 'gtm.js'\n" +
        "    }); var f = d.getElementsByTagName(s)[0],\n" +
        "\n" +
        "    j = d.createElement(s), dl = l != 'dataLayer' ? '&l=' + l : ''; j.async = true; j.src =\n" +
        "\n" +
        "    '//www.googletagmanager.com/gtm.js?id=' + i + dl; f.parentNode.insertBefore(j, f);\n" +
        "\n" +
        "})(window, document, 'script', 'dataLayer', 'GTM-55MWRN');</script>\n" +
        "    <!-- End Google Tag Manager -->\n" +
        "\n" +
        "    <!-- default layout start-->\n" +
        "    <div id=\"DefaultLayoutDiv\">\n" +
        "        <table cellpadding=\"0\" cellspacing=\"0\" width=\"100%\" class=\"top-bar\">\n" +
        "            <tr>\n" +
        "                <td>                    \n" +
        "                    <div id=\"petango_ad\">\n" +
        "                        <a href=\"http://petango.com\" id=\"petango_ad_link\" target=\"_blank\">\n" +
        "                            <img src=\"../adoptablesearch/images/Powered-by-PP.GIF\" alt=\"Powered by Petango\" border=\"0\" />\n" +
        "                        </a>\n" +
        "                    </div>\n" +
        "                </td>\n" +
        "                <td class=\"detail-link\" style=\"text-align: right; vertical-align: top;\">\n" +
        "                    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "                    <div class=\"addthis_inline_share_toolbox\"></div>\n" +
        "                    <a href=\"javascript:window.print()\" class=\"print_btn_container\"><span class=\"print_btn\" /></a>\n" +
        "                    &nbsp;&nbsp;&nbsp;\n" +
        "                    <a href=\"javascript:window.close();\" id=\"BackAnchor\">\n" +
        "                        <img id=\"imgReturnToListing\" src=\"images/closethiswindow.gif\" align=\"top\" /></a>\n" +
        "                    \n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>\n" +
        "        <table border=\"0\">\n" +
        "            <tr>\n" +
        "                <td class=\"detail-animal-name\" colspan=\"2\">\n" +
        "                    Meet <span id=\"lbName\">Puff</span>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "            <tr>\n" +
        "                <td valign=\"top\" style=\"width: 1%\">\n" +
        "                    <img id=\"imgAnimalPhoto\" class=\"detail-animal-photo\" name=\"imgAnimalPhoto\" src=\"//g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8.jpg\" />\n" +
        "                    <div id=\"plPhotos\" class=\"detail-photo-links\">\n" +
        "\t\n" +
        "                        Click a number to change picture or play to see a video:<br />\n" +
        "                        [<a id=\"lnkPhoto1\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/2af51392-1acb-4a34-b111-8f7ac2d37fe8.jpg\">1</a>] [<a id=\"lnkPhoto2\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/bd1e57a1-919a-489d-9ba0-43420863894c.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/bd1e57a1-919a-489d-9ba0-43420863894c.jpg\">2</a>] [<a id=\"lnkPhoto3\" onclick=\"loadPhoto(&#39;//g.petango.com/photos/2681/55cc0a22-5cd2-4d32-86d8-3ad5179d76c2.jpg&#39;); return false;\" href=\"//g.petango.com/photos/2681/55cc0a22-5cd2-4d32-86d8-3ad5179d76c2.jpg\">3</a>]\n" +
        "                        <span id=\"spanVideo\">[<a id=\"lnkVideo\">Play</a>]\n" +
        "                        </span>\n" +
        "                    \n" +
        "</div>\n" +
        "                </td>\n" +
        "                <td valign=\"top\" style=\"width: 100%\">\n" +
        "                    <table class=\"detail-table\" border=\"0\" cellspacing=\"0\">\n" +
        "                        <tr id=\"trAnimalID\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Animal ID</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblID\">39763006</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSpecies\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Species</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSpecies\">Reptile</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trBreed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Breed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbBreed\">Lizard</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trAge\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Age</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbAge\"></span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSex\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Gender</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbSex\">Male</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trSize\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Size</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSize\">Medium</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trColor\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Color</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblColor\">Grey/Tan</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        <tr id=\"trDeclawed\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Declawed</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lbDeclawed\">No</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        <tr id=\"trSite\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Site</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblSite\">Seattle Animal Shelter</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        <tr id=\"trLocation\">\n" +
        "\t<td class=\"detail-label\" align=\"right\">\n" +
        "                                <b>Location</b>\n" +
        "                            </td>\n" +
        "\t<td class=\"detail-value\">\n" +
        "                                <span id=\"lblLocation\">Foster Care</span>&nbsp;\n" +
        "                            </td>\n" +
        "</tr>\n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </table>\n" +
        "                </td>\n" +
        "            </tr>\n" +
        "        </table>        \n" +
        "        <table bgcolor=\"#ffffff\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">\n" +
        "            <tbody>\n" +
        "                <tr>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c1\" src=\"images/Animal_r1_c1.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c2\" src=\"images/Animal_r1_c2.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td height=\"1\">\n" +
        "                        <img name=\"Animal_r1_c3\" src=\"images/Animal_r1_c3.gif\" alt=\"\" border=\"0\" height=\"22\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td background=\"images/Animal_r2_c1.gif\"></td>\n" +
        "                    <td bgcolor=\"#eceff6\" valign=\"top\" style=\"padding-top: 3px\">\n" +
        "                        <!-- Description -->\n" +
        "                        <div id=\"tblDescription\" class=\"detail-animal-desc\">\n" +
        "                            <span id=\"lbDescription\">Puff is an amazing Bearded Dragon and he is looking for a great new forever home! Puff is very shy and a little bit unsocialized so he will need a lot of patience and time to get him to be the wonderful beardie that he is! Because of his special needs, he will have to be adopted by an experienced beardie family!<br/><br/>Puff is currently being adored in foster care!<br/><br/>HOW TO ADOPT: For foster animals, please fill out a critter adoption form available at the shelter. Then you must meet with an animal care officer IN PERSON at the shelter to review your application. Then we will arrange a day and time when the foster parent and critter will meet you at the Shelter critter room.<br/><br/>The Seattle Animal Shelter is located at 2061 15th Ave W. The shelter is open five days a week (Wednesday-Sunday), 1-6 pm, and closed on Mondays, Tuesdays and legal holidays. After the review, please allow 2-3 business days for the foster parent to reach out to you to set up a meet and greet.</span>\n" +
        "                        </div>\n" +
        "                        <!-- Logos -->\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "\n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                        \n" +
        "                    </td>\n" +
        "                    <td background=\"images/Animal_r2_c3.gif\"></td>\n" +
        "                </tr>\n" +
        "                <tr>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c1\" src=\"images/Animal_r3_c1.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"20\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c2\" src=\"images/Animal_r3_c2.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"100%\" />\n" +
        "                    </td>\n" +
        "                    <td>\n" +
        "                        <img name=\"Animal_r3_c3\" src=\"images/Animal_r3_c3.gif\" alt=\"\" border=\"0\" height=\"23\"\n" +
        "                            width=\"24\" />\n" +
        "                    </td>\n" +
        "                </tr>\n" +
        "            </tbody>\n" +
        "        </table>\n" +
        "    </div>\n" +
        "    <!-- default layout end-->\n" +
        "    <!-- alternate layout start-->\n" +
        "    \n" +
        "    <!-- alternate layout end-->\n" +
        "    <!-- Begin comScore Tag -->\n" +
        "    <script>\n" +
        "        document.write(unescape(\"%3Cscript src='\" + (document.location.protocol == \"https:\" ? \"https://sb\" : \"http://b\") + \".scorecardresearch.com/beacon.js' %3E%3C/script%3E\"));\n" +
        "    </script>\n" +
        "    <script>\n" +
        "        COMSCORE.beacon({\n" +
        "            c1: 2,\n" +
        "            c2: 6745171,\n" +
        "            c3: \"\",\n" +
        "            c4: \"\",\n" +
        "            c5: \"\",\n" +
        "            c6: \"\",\n" +
        "            c15: \"\"\n" +
        "        });\n" +
        "    </script>\n" +
        "    <noscript>\n" +
        "        <img src=\"http://b.scorecardresearch.com/p?c1=2&c2=6745171&c3=&c4=&c5=&c6=&c15=&cj=1\" />\n" +
        "    </noscript>\n" +
        "    <!-- End comScore Tag -->\n" +
        "\n" +
        "    <!-- Go to www.addthis.com/dashboard to customize your tools -->\n" +
        "    <script type=\"text/javascript\" src=\"//s7.addthis.com/js/300/addthis_widget.js#pubid=ra-588b5f256d215fd9\"></script>\n" +
        "</body>\n" +
        "    </html>"
}
