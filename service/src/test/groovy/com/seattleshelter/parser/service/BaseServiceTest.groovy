package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.WebParser
import ratpack.test.exec.ExecHarness
import spock.lang.Specification

class BaseServiceTest extends Specification {

    private final WebParser webParser = Mock()
    private final CatService catService = new CatService(webParser)

    def "should only parse web once for two consecutive get calls"() {
        when: 'We get cats twice'
        ExecHarness.yieldSingle { catService.pets }
        ExecHarness.yieldSingle { catService.pets }

        then: 'We only parse from web once'
        1 * webParser.parseCats() >> [
            new Pet("Bobby", "", 1, "male", "tabby", "2 years", false, false)
        ]
    }

    def "should only parse web once for two consecutive get details calls"() {
        when: 'We get cat details twice'
        ExecHarness.yieldSingle { catService.getPetById(1) }
        ExecHarness.yieldSingle { catService.getPetById(1) }

        then: 'We only parse from web once'
        1 * webParser.parseCat(1) >> new PetDetails("Bobby", [], "", "", 1, "male", "tabby", "2 years", "medium", "orange", true, false, false, false)
    }
}
