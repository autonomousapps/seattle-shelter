package com.seattleshelter.persistence

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.utils.SystemTimeStrategy
import com.seattleshelter.utils.TimeProvider
import com.seattleshelter.utils.TimeStrategy
import ratpack.test.exec.ExecHarness
import spock.lang.Specification

class InMemoryDataStoreTest extends Specification {

    private final InMemoryDataStore dataStore = new InMemoryDataStore()
    private final TimeStrategy timeStrategy = Mock()

    private static final STORE_TIME = 100_000L

    def setup() {
        TimeProvider.timeStrategy = timeStrategy
        timeStrategy.now() >> STORE_TIME

        dataStore.storePets(PETS)
        dataStore.storePet(PET_DETAILS.get(1))
        dataStore.storePet(PET_DETAILS.get(2))
        dataStore.storePet(PET_DETAILS.get(3))
    }

    def cleanup() {
        TimeProvider.timeStrategy = new SystemTimeStrategy()
    }

    def "getPets returns all pets as a new list"() {
        expect:
        def pets = ExecHarness.yieldSingle { dataStore.getPets() }.value
        pets == PETS
        !pets.is(PETS)
        dataStore.getLastUpdated() == STORE_TIME
    }

    def "getPet returns expected pet"() {
        expect:
        def id = 1
        def pet = ExecHarness.yieldSingle { dataStore.getPet(id) }.value
        pet == PET_DETAILS.get(id)
        dataStore.getLastUpdated(id) == STORE_TIME
    }

    private static final List<Pet> PETS = [
        new Pet(
            "Bobby",
            "pic",
            1,
            "male",
            "tabby",
            "2 years",
            false,
            false
        ),
        new Pet(
            "Johnny",
            "pic2",
            2,
            "female",
            "tabby",
            "2 years",
            false,
            false
        ),
        new Pet(
            "Tommy",
            "pic3",
            3,
            "female",
            "tabby",
            "2 years",
            false,
            false
        )
    ]

    private static final Map<Long, PetDetails> PET_DETAILS = new LinkedHashMap<Long, PetDetails>() {
        {
            put(1,
                new PetDetails(
                    "Bobby",
                    ["pic"],
                    "",
                    "",
                    1,
                    "male",
                    "tabby",
                    "2 years",
                    "medium",
                    "orange",
                    true,
                    false,
                    false,
                    false
                )
            )
            put(2,
                new PetDetails(
                    "Bobby",
                    ["pic2"],
                    "",
                    "",
                    2,
                    "female",
                    "tabby",
                    "2 years",
                    "medium",
                    "orange",
                    true,
                    false,
                    false,
                    false
                )
            )
            put(3,
                new PetDetails(
                    "Bobby",
                    ["pic3"],
                    "",
                    "",
                    3,
                    "female",
                    "tabby",
                    "2 years",
                    "medium",
                    "orange",
                    true,
                    false,
                    false,
                    false
                )
            )
        }
    }
}
