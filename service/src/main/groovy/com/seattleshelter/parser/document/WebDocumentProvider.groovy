package com.seattleshelter.parser.document

import groovy.util.logging.Slf4j
import org.jsoup.Jsoup
import org.jsoup.nodes.Document

@Slf4j
class WebDocumentProvider implements DocumentProvider {

    private static final String URL_ROOT = "http://ws.petango.com/webservices/adoptablesearch"
    private static final String URL_DETAILS = "$URL_ROOT/wsAdoptableAnimalDetails.aspx?id="

    @Override
    Document getListFor(Pet pet) throws IOException {
        def url = getUrlFor(pet)
        log.info "Getting list for pet ${pet.name()}, at url $url"
        return Jsoup.connect(url).get()
    }

    @Override
    Document getDetailsFor(long id) throws IOException {
        def url = URL_DETAILS + id
        log.info "Getting pet details for url $url"
        return Jsoup.connect(url).get()
    }

    private static String getUrlFor(Pet pet) {
        return "$URL_ROOT/wsAdoptableAnimals.aspx?species=${pet.name()}&sex=A&agegroup=All&location=&site=&onhold=A&orderby=ID&colnum=1&css=//www.seattle.gov/Documents/Departments/AnimalShelter/sas-styles.css&authkey=x5eowv5twf8po63is6gylhf8r608c75k6ksb48t17nmtjtfyof&recAmount=&detailsInPopup=Yes&featuredPet=Include&stageID="
    }

    enum Pet {
        Cat, Dog, Rabbit, Reptile,

        /**
         * "Small Mammals"
         */
        SmallFurry
    }
}
