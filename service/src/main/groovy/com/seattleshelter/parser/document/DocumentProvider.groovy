package com.seattleshelter.parser.document

import org.jsoup.nodes.Document

interface DocumentProvider {
    Document getListFor(WebDocumentProvider.Pet pet) throws IOException
    Document getDetailsFor(long id) throws IOException
}