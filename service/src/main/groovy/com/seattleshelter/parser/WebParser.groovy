package com.seattleshelter.parser

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.document.DocumentProvider
import com.seattleshelter.parser.document.WebDocumentProvider
import groovy.util.logging.Slf4j
import org.jsoup.nodes.Document
import org.jsoup.nodes.Element

@Slf4j
class WebParser {

    private static final String ADOPTION_PENDING_IMG = "//g.petango.com/photos/2681/0b049704-a571-4aa4-a224-dad2d59e8990"

    private final DocumentProvider documentProvider

    WebParser(DocumentProvider documentProvider) {
        this.documentProvider = documentProvider
    }

    List<Pet> parseCats() {
        try {
            return parsePets(WebDocumentProvider.Pet.Cat)
        } catch (IOException e) {
            log.error "Exception parsing cats", e
            return []
        }
    }

    PetDetails parseCat(long id) {
        try {
            return parsePet(id)
        } catch (IOException e) {
            log.error "Exception parsing cat for id <$id>", e
            return null
        }
    }

    List<Pet> parseDogs() {
        try {
            return parsePets(WebDocumentProvider.Pet.Dog)
        } catch (IOException e) {
            log.error "Exception parsing dogs", e
            return []
        }
    }

    PetDetails parseDog(long id) {
        try {
            return parsePet(id)
        } catch (IOException e) {
            log.error "Exception parsing dog for id <$id>", e
            return null
        }
    }

    List<Pet> parseRabbits() {
        try {
            return parsePets(WebDocumentProvider.Pet.Rabbit)
        } catch (IOException e) {
            log.error "Exception parsing rabbits", e
            return []
        }
    }

    PetDetails parseRabbit(long id) {
        try {
            return parsePet(id)
        } catch (IOException e) {
            log.error "Exception parsing rabbit for id <$id>", e
            return null
        }
    }

    List<Pet> parseSmallMammals() {
        try {
            return parsePets(WebDocumentProvider.Pet.SmallFurry)
        } catch (IOException e) {
            log.error "Exception parsing small mammals", e
            return []
        }
    }

    PetDetails parseSmallMammal(long id) {
        try {
            return parsePet(id)
        } catch (IOException e) {
            log.error "Exception parsing small mammal for id <$id>", e
            return null
        }
    }

    List<Pet> parseReptiles() {
        try {
            return parsePets(WebDocumentProvider.Pet.Reptile)
        } catch (IOException e) {
            log.error "Exception parsing reptiles", e
            return []
        }
    }

    PetDetails parseReptile(long id) {
        try {
            return parsePet(id)
        } catch (IOException e) {
            log.error "Exception parsing reptile for id <$id>", e
            return null
        }
    }

    private List<Pet> parsePets(WebDocumentProvider.Pet pet) {
        List<Pet> pets = []

        documentProvider.getListFor(pet).getElementsByClass('list-item').each { row ->
            def picUrl = findPicUrl(row)
            def name = findName(row)
            def id = findId(row)
            def sex = findSex(row)
            def breed = findBreed(row)
            def age = findAge(row)
            def inFosterCare = findInFosterCare(row)
            def adoptionPending = picUrl.contains(ADOPTION_PENDING_IMG)
            pets << new Pet(name, picUrl, id, sex, breed, age, inFosterCare, adoptionPending)
        }

        return pets
    }

    private PetDetails parsePet(long id) {
        def doc = documentProvider.getDetailsFor(id)
        def name = findDetailsName(doc)
        def picUrls = findDetailsPicUrls(doc)
        def videoUrl = findDetailsVideoUrl(doc)
        def description = findDetailsDescription(doc)
        def breed = findDetailsBreed(doc)
        def age = findDetailsAge(doc)
        def sex = findDetailsSex(doc)
        def size = findDetailsSize(doc)
        def color = findDetailsColor(doc)
        def altered = findDetailsAltered(doc)
        def declawed = findDetailsDeclawed(doc)
        def inFosterCare = findDetailsInFosterCare(doc)
        def adoptionPending = picUrls.find { it.contains(ADOPTION_PENDING_IMG) } != null
        return new PetDetails(name, picUrls, videoUrl, description, id, sex, breed, age, size, color, altered, declawed, inFosterCare, adoptionPending)
    }

    private static String findPicUrl(Element row) {
        "http:${row.getElementsByClass('list-animal-photo').attr('src').replaceAll('_TN1', '')}"
    }

    private static String findName(Element row) {
        row.getElementsByClass('list-animal-name').first().getElementsByTag('a').first().text().replaceAll(' NEW INFO', '')
    }

    private static long findId(Element row) {
        def id = ""
        try {
            id = row.getElementsByClass('list-animal-id').first().text()
            return Long.parseLong(id)
        } catch (NumberFormatException e) {
            log.error "NumberFormatException parsing cat id <$id>; returning -1", e
            return -1
        }
    }

    private static String findSex(Element row) {
        row.getElementsByClass('list-animal-sexSN').first().text()
    }

    private static String findBreed(Element row) {
        row.getElementsByClass('list-animal-breed').first().text()
    }

    private static String findAge(Element row) {
        row.getElementsByClass('list-animal-age').first().text()
    }

    private static boolean findInFosterCare(Element row) {
        row.getElementsByClass('hidden').first().text() == 'Foster Care'
    }

    private static String findDetailsName(Document doc) {
        doc.getElementById('lbName').text()
    }

    private static List<String> findDetailsPicUrls(Document doc) {
        // TODO sometimes this can be empty, even though we still have a photo of the cat. Fall back on that
        def one = doc.getElementById('lnkPhoto1')?.attr('href') ?: ''
        def two = doc.getElementById('lnkPhoto2')?.attr('href') ?: ''
        def three = doc.getElementById('lnkPhoto3')?.attr('href') ?: ''
        return [one, two, three].findAll { !it.isEmpty() }.collect { "http:$it".toString() }
    }

    private static String findDetailsVideoUrl(Document doc) {
        def link = doc.getElementById('lnkVideo')?.attr('href') ?: ''
        if (!link.isEmpty()) {
            return 'https://youtu.be/' + link
        } else {
            return ''
        }
    }

    private static String findDetailsDescription(Document doc) {
        doc.getElementById('lbDescription').text()
    }

    private static String findDetailsBreed(Document doc) {
        doc.getElementById('lbBreed').text()
    }

    private static String findDetailsAge(Document doc) {
        doc.getElementById('lbAge').text()
    }

    private static String findDetailsSex(Document doc) {
        doc.getElementById('lbSex').text()
    }

    private static String findDetailsSize(Document doc) {
        doc.getElementById('lblSize').text()
    }

    private static String findDetailsColor(Document doc) {
        doc.getElementById('lblColor').text()
    }

    private static boolean findDetailsAltered(Document doc) {
        doc.getElementById('trAltered')?.getElementsByTag('img')?.attr('src') == 'images/GreenCheck.JPG'
    }

    private static boolean findDetailsDeclawed(Document doc) {
        doc.getElementById('lbDeclawed').text() != 'No'
    }

    // There are three known values:
    // 1. Cat Free Roaming Room
    // 2. Foster Care
    // 3. Cat Adoption
    private static boolean findDetailsInFosterCare(Document doc) {
        doc.getElementById('lblLocation').text() == 'Foster Care'
    }
}
