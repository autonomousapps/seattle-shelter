package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.WebParser
import com.seattleshelter.persistence.DataStore
import com.seattleshelter.persistence.InMemoryDataStore
import groovy.util.logging.Slf4j
import ratpack.exec.Promise

@Slf4j
class DogService extends BaseService {

    private final DataStore dataStore = new InMemoryDataStore()

    DogService(WebParser webParser) {
        super(webParser)
    }

    @Override
    Promise<List<Pet>> getPets() {
        return pets(dataStore) { webParser.parseDogs() }
    }

    @Override
    Promise<PetDetails> getPetById(long id) {
        log.info "looking for dog details for $id"
        return petDetails(dataStore, id) { webParser.parseDog(id) }
    }
}
