package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.WebParser
import com.seattleshelter.persistence.DataStore
import com.seattleshelter.utils.TimeProvider
import groovy.util.logging.Slf4j
import ratpack.exec.Promise

@Slf4j
abstract class BaseService implements PetService {

    protected static final long ONE_HOUR = 60 * 60 * 1000

    protected final WebParser webParser

    BaseService(WebParser webParser) {
        this.webParser = webParser
    }

    protected static Promise<List<Pet>> pets(DataStore dataStore, Closure<List<Pet>> parseCall) {
        return dataStore.getPets().flatMap { storedPets ->
            long lastUpdated = dataStore.getLastUpdated()
            long now = TimeProvider.now()
            if (storedPets.isEmpty() || now - lastUpdated > ONE_HOUR) {
                def parsedPets = parseCall()
                dataStore.storePets(parsedPets)
                return Promise.value(parsedPets)
            } else {
                return Promise.value(storedPets)
            }
        }
    }

    protected static Promise<PetDetails> petDetails(DataStore dataStore, long id, Closure<PetDetails> parseCall) {
        dataStore.getPet(id).flatMap { storedPet ->
            long lastUpdated = dataStore.getLastUpdated(id)
            long now = TimeProvider.now()
            if (storedPet == null || now - lastUpdated > ONE_HOUR) {
                def parsedPet = parseCall()
                dataStore.storePet(parsedPet)
                return Promise.value(parsedPet)
            } else {
                return Promise.value(storedPet)
            }
        }
    }
}
