package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.WebParser
import com.seattleshelter.persistence.DataStore
import com.seattleshelter.persistence.InMemoryDataStore
import groovy.util.logging.Slf4j
import ratpack.exec.Promise

@Slf4j
class CatService extends BaseService {

    private final DataStore dataStore = new InMemoryDataStore()

    CatService(WebParser webParser) {
        super(webParser)
    }

    @Override
    Promise<List<Pet>> getPets() {
        return pets(dataStore) { webParser.parseCats() }
    }

    @Override
    Promise<PetDetails> getPetById(long id) {
        log.info "looking for cat details for $id"
        return petDetails(dataStore, id) { webParser.parseCat(id) }
    }
}
