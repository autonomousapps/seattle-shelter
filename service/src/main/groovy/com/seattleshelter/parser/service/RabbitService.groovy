package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.parser.WebParser
import com.seattleshelter.persistence.DataStore
import com.seattleshelter.persistence.InMemoryDataStore
import groovy.util.logging.Slf4j
import ratpack.exec.Promise

@Slf4j
class RabbitService extends BaseService {

    private final DataStore dataStore = new InMemoryDataStore()

    RabbitService(WebParser webParser) {
        super(webParser)
    }

    @Override
    Promise<List<Pet>> getPets() {
        return pets(dataStore) { webParser.parseRabbits() }
    }

    @Override
    Promise<PetDetails> getPetById(long id) {
        log.info "looking for rabbit details for $id"
        return petDetails(dataStore, id) { webParser.parseRabbit(id) }
    }
}
