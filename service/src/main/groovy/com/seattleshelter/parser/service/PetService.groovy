package com.seattleshelter.parser.service

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import ratpack.exec.Promise

interface PetService {
    Promise<List<Pet>> getPets()
    Promise<PetDetails> getPetById(long id)
}