package com.seattleshelter.persistence

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import com.seattleshelter.utils.TimeProvider
import ratpack.exec.Operation
import ratpack.exec.Promise

final class InMemoryDataStore implements DataStore {

    private final List<Pet> _pets = new ArrayList<>()
    private long _lastUpdated = 0L

    private final Map<Long, PetDetails> _petDetails = new LinkedHashMap<>()
    private final Map<Long, Long> _lastUpdatedDetails = new LinkedHashMap<>()

    @Override
    Promise<List<Pet>> getPets() {
        synchronized (_pets) {
            return Promise.value(_pets.collect())
        }
    }

    @Override
    Operation storePets(List<Pet> cats) {
        synchronized (_pets) {
            _pets.with {
                clear()
                addAll(cats)
            }
            _lastUpdated = TimeProvider.now()
            return Operation.noop()
        }
    }

    @Override
    long getLastUpdated() {
        synchronized (_pets) {
            return _lastUpdated
        }
    }

    @Override
    Promise<PetDetails> getPet(long id) {
        synchronized (_petDetails) {
            return Promise.value(_petDetails.get(id))
        }
    }

    @Override
    Operation storePet(PetDetails petDetails) {
        synchronized (_petDetails) {
            _petDetails.put(petDetails.id, petDetails)
            _lastUpdatedDetails.put(petDetails.id, TimeProvider.now())
            return Operation.noop()
        }
    }

    @Override
    long getLastUpdated(long id) {
        synchronized (_petDetails) {
            return _lastUpdatedDetails.get(id) ?: 0L
        }
    }
}
