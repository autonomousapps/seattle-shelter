package com.seattleshelter.persistence

import com.seattleshelter.entities.Pet
import com.seattleshelter.entities.PetDetails
import ratpack.exec.Operation
import ratpack.exec.Promise

interface DataStore {

    // List
    Promise<List<Pet>> getPets()
    Operation storePets(List<Pet> pets)
    long getLastUpdated()

    // ID
    Promise<PetDetails> getPet(long id)
    Operation storePet(PetDetails petDetails)
    long getLastUpdated(long id)
}