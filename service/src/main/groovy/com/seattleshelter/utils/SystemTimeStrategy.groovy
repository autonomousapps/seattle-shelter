package com.seattleshelter.utils

class SystemTimeStrategy implements TimeStrategy {
    @Override
    long now() {
        return System.currentTimeMillis()
    }
}
