package com.seattleshelter.utils

interface TimeStrategy {
    long now()
}