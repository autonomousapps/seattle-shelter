package com.seattleshelter.utils

class TimeProvider {

    static TimeStrategy timeStrategy = new SystemTimeStrategy()

    static long now() {
        return timeStrategy.now()
    }
}